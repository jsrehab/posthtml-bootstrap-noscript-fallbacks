#!/usr/bin/env sh

set -o errexit
set -o nounset

bootstrap_repo="${1:?Specify the Bootstrap repository directory}"
output_dir="${2:?Specify an output directory}"

components='alert carousel collapse dropdown modal popover tab toast tooltip'

for component in ${components}; do
  html="<!-- Extracted from Bootstrap's repo documentation (MIT license): https://github.com/twbs/bootstrap/tree/main/site/content/docs/5.1/components -->"

  html="${html}\n\n$(cat "${bootstrap_repo}/js/tests/visual/${component}.html")"

  html="${html}\n\n<link rel=\"stylesheet\" href=\"../../node_modules/bootstrap/dist/css/bootstrap.min.css\">"
  html="${html}\n\n<script src=\"../../node_modules/bootstrap/dist/js/bootstrap.min.js\"></script>"
  echo "${html}" > "${output_dir}/bootstrap_visual_test_${component}.html"
done
