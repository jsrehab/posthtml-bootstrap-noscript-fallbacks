# Collapse Fallbacks

[Bootstrap 5 collapse
component](https://getbootstrap.com/docs/5.1/components/collapse/)

## Implementation details

These fallbacks use checkboxes.

## Available Features

- Collapse buttons toggle the visibility of associated collapsible element
- Collapse buttons are activated with Space
- Supports `<button>` and `<a>` tags
- Button focus (`:focus`) indicator
- Button hover (`:hover`) appearance change
- Button activation (`:active`) appearance change
- Focus stays on the collapse button after activation

## Differences with Bootstrap 5

- Buttons cannot be activated with Enter, only Space
- Collapse button can only target a single element (TODO)
- No collapsing animation (TODO)
- WAI-ARIA state attribute `aria-expanded` on collapse buttons is not present
  (and not updated)
- Appearance change of accordions using collapse (so that more than one
  accordion item can be open at once) is not supported (FIXME)
