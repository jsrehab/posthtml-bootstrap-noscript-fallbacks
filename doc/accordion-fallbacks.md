# Accordion Fallbacks

[Bootstrap 5 accordion
component](https://getbootstrap.com/docs/5.1/components/accordion/)

## Implementation details

These fallbacks use radio buttons.

## Available Features

- Accordion item headers toggle the visibility of associated accordion item
  bodies
- Navigation to and from the accordion uses Tab while activation of
  accordion items uses arrow keys
- Accordion items headers are automatically activated when receiving focus
- When returning to a previously navigated accordion, the last accordion item
  header activated is focused
- Zero or one accordion item can be opened at once
- Accordion item header focus (`:focus`) indicator
- Focus stays on the accordion item header after activation

## Differences with Bootstrap 5

- Once open, it is not possible to have all accordion items closed at the same
  time
- No collapsing animation (TODO)
- WAI-ARIA state attribute `aria-expanded` on accordion item headers  is not
  present (and not updated)
