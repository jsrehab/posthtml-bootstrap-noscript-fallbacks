# Tooltips Fallbacks

[Bootstrap 5 tooltips
component](https://getbootstrap.com/docs/5.1/components/tooltips/)

## Implementation details

These fallbacks use `:hover` and `:focus` styling.

## Available Features

- The tooltip associated to the element is shown when the element is hovered or
  focused
- Tooltip placement strictly obeys the element tooltip configuration (their
  placement cannot be adjusted according to the viewport)
- Tooltips on disabled elements are supported

## Differences With Bootstrap 5

- No dynamic placement by taking the viewport into account
- Tooltips may be less wide, especially tooltips placed on the left and right,
  forcing tooltip content wrapping (FIXME)
- Tooltip position may be slightly different, especially for tooltips placed on
  the left and right (FIXME)
- Tooltip alignment may be wrong for disabled elements (FIXME)
- For now, HTML is not escaped (TODO)
