# Carousels Fallbacks

[Bootstrap 5 carousel
component](https://getbootstrap.com/docs/5.1/components/carousel/)

## Implementation details

These fallbacks use radio buttons.

## Available Features

- Carousels can be operated with arrow buttons
- Carousels can be operated with indicator buttons
- Navigation to and from the carousel uses Tab while activation of
  carousel slides uses arrow keys
- Current slide is denoted by indicators

## Differences With Bootstrap 5

- Buttons cannot be activated with Enter, only Space
- Carousels do not autoplay (TODO)
- Sliding animations are supported (TODO)
- Arrow and indicator buttons cannot be focused (since keyboard navigation is
  entirely done with arrow keys)
- Carousels cannot be operated by touch swiping
