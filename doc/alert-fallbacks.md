# Alert Fallbacks

[Bootstrap 5 alert
component](https://getbootstrap.com/docs/5.1/components/alerts/)

## Implementation details

These fallbacks use checkboxes.

## Available Features

- Alerts can be dismissed by activating their optional close button
- Alert dismissal is triggered with Space
- Supports `<button>` and `<a>` tags
- Button focus (`:focus`) indicator
- Button hover (`:hover`) appearance change
- Button activation (`:active`) appearance change
- After dismissal, the next Tab press focuses the next focusable element

## Differences with Bootstrap 5

- Buttons cannot be activated with Enter, only Space
- No dismissal animation (TODO)
- After dismissal, the focus is not lost and instead the next Tab press focuses
  the next focusable element
