# Tabs Fallbacks

[Bootstrap 5 tabs
component](https://getbootstrap.com/docs/5.1/components/navs-tabs/)

[Bootstrap 5 list group
component](https://getbootstrap.com/docs/5.1/components/list-group/)

Two different implementations are provided as tabs fallbacks.

## Radio Implementation

### Available Features

#### Tabs

- Associated tab panel is shown when a tab is activated
- Tabs are changed to their active styled when activated
- Navigation to and from the tab list uses Tab while activation of tabs uses
  arrow keys
- Tabs are automatically activated when receiving focus, as recommended in
  [WAI-ARIA Authoring
  Practices 1.2](https://w3c.github.io/aria-practices/#keyboard-interaction-20)
- When returning to a previously navigated tab list, the last tab activated is
  focused
- Tab hover (`:hover`) appearance change
- Button focus (`:focus`) indicator (not present in Bootstrap) (FIXME: improve
  its appearance, and make more visually accessible if needed)
- Supports `<button>` and `<a>` tags
- User text selection is disabled on tabs
- Zero or one tab are activated at all time, when no tab are activated, only the
  tab list is shown

#### Pills

- Associated tab panel is shown when a pill is activated
- Pills are changed to their active styled when activated
- Navigation to and from the tab list uses Tab while activation of pills uses
  arrow keys
- Pills are automatically activated when receiving focus, as recommended in
  [WAI-ARIA Authoring
  Practices 1.2 Section 3.23](https://w3c.github.io/aria-practices/#keyboard-interaction-20)
- When returning to a previously navigated tab list, the last pill activated is
  focused
- Button focus (`:focus`) indicator (not present in Bootstrap) (FIXME: improve
  its appearance, and make more visually accessible if needed)
- Supports `<button>` and `<a>` tags
- User text selection is disabled on pills
- Zero or one pill are activated at all time, when no pill are activated, only
  the tab list is shown

#### Lists

- Associated tab panel is shown when a list item is activated
- List items are changed to their active styled when activated
- Navigation to and from the tab list uses Tab while activation of list items
  uses arrow keys
- List items are automatically activated when receiving focus, as recommended in
  [WAI-ARIA Authoring
  Practices 1.2](https://w3c.github.io/aria-practices/#keyboard-interaction-20)
- When returning to a previously navigated tab list, the last list item
  activated is focused
- Button focus (`:focus`) indicator (not present in Bootstrap) (FIXME: improve
  its appearance, and make more visually accessible if needed)
- Supports `<button>` and `<a>` tags
- User text selection is disabled on list items
- Zero or one list item are activated at all time, when no list item are
  activated, only the tab list is shown

### Differences With Bootstrap 5

#### Tabs

- Navigation between tabs, pills and list items uses arrow keys, not Tab and
  Space/Enter, and they are automatically activated
- The Tab sequence only includes one tab list item, instead of every tabs, as
  recommended in
  [WAI-ARIA Authoring Practices 1.2
  Section 6.6](https://w3c.github.io/aria-practices/#kbd_general_within)
- Activation of tab panels is not animated (TODO)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on tabs are not
  present (and not updated)

#### Pills

- Navigation between tabs, pills and list items uses arrow keys, not Tab and
  Space/Enter, and they are automatically activated
- The Tab sequence only includes one tab list item, instead of every tabs, as
  recommended in
  [WAI-ARIA Authoring Practices 1.2
  Section 6.6](https://w3c.github.io/aria-practices/#kbd_general_within)
- Activation of tab panels is not animated (TODO)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on tabs are not
  present (and not updated)

#### Lists

- Navigation between tabs, pills and list items uses arrow keys, not Tab and
  Space/Enter, and they are automatically activated
- The Tab sequence only includes one tab list item, instead of every tabs, as
  recommended in
  [WAI-ARIA Authoring Practices 1.2
  Section 6.6](https://w3c.github.io/aria-practices/#kbd_general_within)
- Activation of tab panels is not animated (TODO)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on tabs are not
  present (and not updated)

## Target-id Implementation

### Available Features

#### Tabs

- Associated tab panel is shown when a tab is activated
- Tabs are changed to their active styled when activated
- Navigation between tabs uses Tab
- Tab hover (`:hover`) appearance change (FIXME)
- Supports `<button>` and `<a>` tags
- User text selection is disabled on tabs
- Zero or one tab are activated at all time, when no tab are activated, only the
  tab list is shown

#### Pills

- Associated tab panel is shown when a pill is activated
- Pills are changed to their active styled when activated
- Navigation between pills uses Tab
- Supports `<button>` and `<a>` tags
- User text selection is disabled on pills
- Zero or one pill are activated at all time, when no pill are activated, only
  the tab list is shown

#### Lists

- Associated tab panel is shown when a list item is activated
- List items are changed to their active styled when activated
- Navigation between list items uses Tab
- Supports `<button>` and `<a>` tags
- User text selection is disabled on list items
- Zero or one list item are activated at all time, when no list item are
  activated, only the tab list is shown

### Differences With Bootstrap 5

#### Tabs

- Activation of tab panels is not animated (TODO, if possible)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on tabs are not
  present (and not updated)
- Since the currently active tab is stored in the URL, only one tab component
  can behave properly; if multiple tab components are present on the same page,
  other tab components will be reset to their initial state when interacting
  with one
- For the same reason, it the page uses anchors to jump to page sections, tab
  components will be reset to their initial state when following the anchor
- For now, tab navigation restarts at the first tab in the tab list after
  activating a tab, which may need to be announced to screen readers to avoid
  unexpected change of context

#### Pills

- Activation of tab panels is not animated (TODO, if possible)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on pills are not
  present (and not updated)
- Since the currently active pill is stored in the URL, only one pill component
  can behave properly; if multiple pill components are present on the same page,
  other pill components will be reset to their initial state when interacting
  with one
- For the same reason, it the page uses anchors to jump to page sections, pill
  components will be reset to their initial state when following the anchor
- For now, pill navigation restarts at the first pill in the tab list after
  activating a pill, which may need to be announced to screen readers to avoid
  unexpected change of context

#### Lists

- Activation of tab panels is not animated (TODO, if possible)
- WAI-ARIA state attributes `aria-selected` and `aria-current` on list items are
  not present (and not updated)
- Since the currently active list item is stored in the URL, only one list
  component can behave properly; if multiple list components are present on the
  same page, other list components will be reset to their initial state when
  interacting with one
- For the same reason, it the page uses anchors to jump to page sections, list
  components will be reset to their initial state when following the anchor
- For now, list item navigation restarts at the first list item in the tab list
  after activating a list item, which may need to be announced to screen readers
  to avoid unexpected change of context
