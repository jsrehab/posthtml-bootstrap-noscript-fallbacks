# Popovers Fallbacks

[Bootstrap 5 popovers
component](https://getbootstrap.com/docs/5.1/components/popovers/)

## Implementation details

These fallbacks use checkboxes.

## Available Features

- Popovers visibility is toggle by activating the associated element
- Popovers placement strictly obeys the element popover configuration (their
  placement cannot be adjusted according to the viewport)
- Popovers on disabled elements are supported

## Differences With Bootstrap 5

- No dynamic placement by taking the viewport into account
- Popovers may be less wide, especially popovers positioned on the left and
  right, forcing popover content wrapping (FIXME)
- Popovers position may be slightly different, especially for popovers
  placed on the left and right (FIXME)
- For now, HTML is not escaped (TODO)
