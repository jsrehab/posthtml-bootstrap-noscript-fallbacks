# Dropdown Fallbacks

[Bootstrap 5 dropdown
component](https://getbootstrap.com/docs/5.1/components/dropdowns/)

## Implementation details

These fallbacks use checkboxes.

## Available Features

- Dropdown menu being shown/hidden when activating the button
- Dropdown are activated with Space
- Supports `<button>` and `<a>` tags
- Button focus (`:focus`) indicator
- Button hover (`:hover`) appearance change
- Button activation (`:active`) appearance change
- Dropdown menu auto-close behavior (FIXME: test accessibility)
- 4 dropdown directions
- Both dropdown menu alignment for non-split button dropdowns
- All content can be accessed using Tab, including navigating to and from the
  dropdown menu
- Focus is not automatically moved to the dropdown menu when opening it

## Differences With Bootstrap 5

- Buttons cannot be activated with Enter, only Space
- No dynamic positioning by taking the viewport into account
- No keyboard navigation using Escape (to close the dropdown) and arrow keys (to
  navigate between dropdown menu items)
- Dropdown cannot be opened with the down arrow key
> FIXME split button rounder corners, arrow vertical alignment, split dropdown
> horizontal alignment, responsive alignment, RTL modes, dropdown menu offset
- Focus in not trapped inside the dropdown menu


## Unsupported features of older Bootstrap versions
- Default styling of navbar dropdowns (based on `<a>` tags) from Bootstrap < 4
  is unsupported.
