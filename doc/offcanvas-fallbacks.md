# Offcanvas Fallbacks

[Bootstrap 5 offcanvas
component](https://getbootstrap.com/docs/5.1/components/offcanvas/)

## Implementation details

These fallbacks use checkboxes.

## Available Features

- Offcanvas being shown/hidden when activating the associated button
- Offcanvas being hidden when activating the close button
- Buttons triggering offcanvases are activated with Space
- Supports `<button>` and `<a>` tags for opening and closing buttons
- Button focus (`:focus`) indicator on opening and closing buttons
- Button hover (`:hover`) appearance change on opening and closing buttons
- Button activation (`:active`) appearance change on opening buttons
- 4 offcanvas placements
- Focus is not automatically moved to the offcanvas when opening it
- Backdrop is shown with the offcanvas and can be clicked to close it
- Multiple buttons can trigger the same offcanvas

## Differences With Bootstrap 5

- Buttons cannot be activated with Enter, only Space
- Focus is given to the close button after opening the offcanvas
- Page scrolling is not forbidden when the offcanvas is open
- Focus in not trapped inside the offcanvas
- No closing animation (FIXME)
