#!/usr/bin/env sh

set -o errexit
set -o nounset

bootstrap_repo="${1:?Specify the Bootstrap repository directory}"
output_dir="${2:?Specify an output directory}"

components='accordion alerts carousel collapse dropdowns modal navs-tabs offcanvas popovers toasts tooltips'

for component in ${components}; do
  html="<!-- Extracted from Bootstrap's repo visual tests (CC BY 3.0 license): https://github.com/twbs/bootstrap/tree/main/js/tests/visual -->"

  html="${html}\n\n$(awk '/{{< example( class="bg-light")? >}}|^<div class="bd-example">|^```html$/,/{{< \/example >}}|^<\/div>\n|^```$/' \
    "${bootstrap_repo}/site/content/docs/5.1/components/${component}.md" \
      | sed 's/{{< \/\?example\( class="bg-light"\)\? >}}\|^```html$\|^```$//')"

  html="${html}\n\n<link rel=\"stylesheet\" href=\"../../node_modules/bootstrap/dist/css/bootstrap.min.css\">"
  html="${html}\n\n<script src=\"../../node_modules/@popperjs/core/dist/umd/popper.min.js\"></script>"
  html="${html}\n\n<script src=\"../../node_modules/bootstrap/dist/js/bootstrap.min.js\"></script>"
  echo "${html}" > "${output_dir}/bootstrap_doc_example_${component}.html"
done
