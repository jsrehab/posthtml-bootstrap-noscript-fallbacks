/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  getNodeAttribute,
  setNodeAttribute,
  mimicNode,
  hasClass,
  getFrameworkStyle,
  getMatchingNode,
  findCommonAncestor,
  findChildIndex,
  insertNodesAround,
  closestNode,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const BOOTSTRAP_ACTIVE_CLASS = 'active';

const NSF_TAB_GROUP_DATA_ATTR = 'data-nsf-tabgroup';

const getFallbackNodeRadio = (
  containerTag,
  button,
  buttonClass,
  tabPaneTarget,
  tabGroup,
  isActive,
  componentStyle,
) => {
  const radioGroup = tabGroup;

  const labelClass = getRandomClass();
  const radioId = getRandomClass();

  const checkedAttributeStr = isActive ? 'checked' : '';

  // display: flex makes sure inputs are closer to tabs
  // autocomplete="off" prevents Firefox from preserving input state across
  // page refresses
  // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
  const groupNodes = parser(`
    <input type="radio" id="${radioId}" name="${radioGroup}" autocomplete="off"
      style="display:flex;position:fixed;opacity:0" ${checkedAttributeStr}>
  `);

  const fallbackNode = parser(`
    <label role="button" for="${radioId}" style="display:none"></label>

    <${containerTag}>
      <style>
        .${buttonClass} {
          display: none !important;
        }

        .${labelClass} {
          display: inline-block !important;
          cursor: pointer;
          user-select: none;
        }

        /* Fix round corners for the first button */
        .list-group-item:nth-child(2) {
            border-top-left-radius: inherit;
            border-top-right-radius: inherit;
        }

        /* Remove Bootstrap active styling */
        .nav-tabs label.nav-link.active {
          ${componentStyle.tab.inactive}
        }
        .nav-pills label.nav-link.active {
          ${componentStyle.pill.inactive}
        }
        label.list-group-item.active {
          ${componentStyle.list.inactive}
        }

        /* Remove Bootstrap active styling */
        .tab-content > .tab-pane.active {
          display: none;
        }

        /* This must have a higher specificity than the rule above removing
         * styling */
        #${radioId}:checked ~ .nav-tabs .${labelClass}.nav-link,
        #${radioId}:checked ~ * .nav-tabs .${labelClass}.nav-link {
          ${componentStyle.tab.active}
        }
        #${radioId}:checked ~ .nav-pills .${labelClass}.nav-link,
        #${radioId}:checked ~ * .nav-pills .${labelClass}.nav-link {
          ${componentStyle.pill.active}
        }
        #${radioId}:checked ~ * .${labelClass}.list-group-item {
          ${componentStyle.list.active}
        }

        #${radioId}:checked ~ * ${tabPaneTarget} {
          display: block;
          opacity: 1;
        }

        /* Make the focus visible */
        #${radioId}:focus-visible ~ * .${labelClass} {
          outline: 2px dotted;
        }
      </style>
    </${containerTag}>
  `);

  const label = mimicNode(fallbackNode, 'label', button);
  appendClass(label, labelClass);
  appendClass(label, NSF_FALLBACK_CLASS);

  return [fallbackNode, groupNodes];
};

// This method can only be used when every page features at most *one* tablist
const getFallbackNodeTargetId = (
  containerTag,
  button,
  buttonClass,
  tabPaneTarget,
  tabGroup,
  isActive,
  componentStyle,
) => {
  const anchorClass = getRandomClass();

  const divClass = getRandomClass();

  const targetId = tabPaneTarget.slice(1);

  const groupNodes = parser(`
    <div id="${targetId}-tab" class="${tabGroup} ${divClass}"
      style="position:fixed"></div>
  `);

  const fallbackNode = parser(`
    <a style="display:none"></a>

    <${containerTag}>
      <style>
        /* Remove the initial button from the document flow */
        .${buttonClass} {
          display: none !important;
        }

        .${anchorClass} {
          display: inline-block !important;
          user-select: none;
        }

        /* Fix round corners for the first button */
        .list-group-item:nth-child(2) {
            border-top-left-radius: inherit;
            border-top-right-radius: inherit;
        }

        .${tabGroup} ~ .nav-tabs .${anchorClass}:not(.active).nav-link,
        .${tabGroup} ~ * .nav-tabs .${anchorClass}:not(.active).nav-link,
        .${tabGroup}:target:not(.${divClass}:target) ~ .nav-tabs .${anchorClass}.active.nav-link,
        .${tabGroup}:target:not(.${divClass}:target) ~ * .nav-tabs .${anchorClass}.active.nav-link {
          ${componentStyle.tab.inactive}
        }
        .${tabGroup} ~ .nav-pills .${anchorClass}:not(.active).nav-link,
        .${tabGroup} ~ * .nav-pills .${anchorClass}:not(.active).nav-link,
        .${tabGroup}:target:not(.${divClass}:target) ~ .nav-pills .${anchorClass}.active.nav-link,
        .${tabGroup}:target:not(.${divClass}:target) ~ * .nav-pills .${anchorClass}.active.nav-link {
          ${componentStyle.pill.inactive}
        }
        .${tabGroup} ~ * .${anchorClass}:not(.active).list-group-item,
        .${tabGroup}:target:not(.${divClass}:target) ~ * .${anchorClass}.active.list-group-item {
          ${componentStyle.list.inactive}
        }

        /* Hide the panel when it is not the one by default (marked .active) or
         * when another panel is shown */
        .${tabGroup} ~ * ${tabPaneTarget}:not(.active),
        .${tabGroup}:target:not(.${divClass}:target) ~ * ${tabPaneTarget}.active {
          display: none;
        }

        .${divClass}:target ~ * ${tabPaneTarget} {
          display: block;
          opacity: 1;
        }

        /* This must have a higher specificity than the rule above removing
         * styling */
        .${divClass}:target ~ .nav-tabs .${anchorClass}.nav-link,
        .${divClass}:target ~ * .nav-tabs .${anchorClass}.nav-link {
          ${componentStyle.tab.active}
        }
        .${divClass}:target ~ .nav-pills .${anchorClass}.nav-link,
        .${divClass}:target ~ * .nav-pills .${anchorClass}.nav-link {
          ${componentStyle.pill.active}
        }
        .${divClass}:target ~ * .${anchorClass}.list-group-item {
          ${componentStyle.list.active}
        }
      </style>
    </${containerTag}>
  `);

  const anchor = mimicNode(fallbackNode, 'a', button);
  // This must be added after mimicking the node, otherwise it could be
  // overwritten by the original href if the original node is an anchor element
  anchor.attrs.href = `#${targetId}-tab`;
  appendClass(anchor, anchorClass);
  appendClass(anchor, NSF_FALLBACK_CLASS);

  return [fallbackNode, groupNodes];
};

const addTabFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  tree.walk(node => {
    if (!node || !node.content || !node.attrs) {
      return node;
    }

    const isTabButton = node.attrs
      && ['tab', 'pill', 'list'].includes((node.attrs['data-toggle']
      || node.attrs['data-bs-toggle']));

    if (!isTabButton) {
      return node;
    }

    const tabButton = node;

    const container = closestNode(tabButton, '.nav, .list-group');

    if (!container) {
      return node;
    }

    let tabGroup = getNodeAttribute(container, NSF_TAB_GROUP_DATA_ATTR);

    if (!tabGroup) {
      tabGroup = `${getRandomClass()}-tab`;
      setNodeAttribute(container, NSF_TAB_GROUP_DATA_ATTR , tabGroup);
    }

    const buttonClass = getRandomClass();

    const tabPaneTarget = (tabButton.attrs['data-target']
      || tabButton.attrs['data-bs-target'])
      || tabButton.attrs.href;

    const tabPane = getMatchingNode(tree, tabPaneTarget);

    let [commonAncestor, firstChild] = [{}, {}];

    if (tabPane) {
      [commonAncestor, firstChild] = findCommonAncestor(
        tabButton,
        tabPane,
      );
    }

    const componentStyle = {
      tab: {
        active: getFrameworkStyle(
          cssRules,
          ['.nav-tabs .nav-link.active'],
          { node: tabButton, class: 'nav-link' },
          [
            'color',
            'background-color',
            'border-color',
          ],
        ),
        inactive: getFrameworkStyle(
          cssRules,
          ['.nav-link', '.nav-tabs .nav-link'],
          { node: tabButton, class: 'nav-link' },
          [
            'color',
            'background',
            'border',
          ],
        ),
      },
      pill: {
        active: getFrameworkStyle(
          cssRules,
          ['.nav-pills .nav-link.active'],
          { node: tabButton, class: 'nav-link' },
          [
            'color',
            'background-color',
          ],
        ),
        inactive: getFrameworkStyle(
          cssRules,
          ['.nav-link', '.nav-pills .nav-link'],
          { node: tabButton, class: 'nav-link' },
          [
            'color',
            'background',
          ],
        ),
      },
      list: {
        active: getFrameworkStyle(
          cssRules,
          ['.list-group-item.active'],
          { node: tabButton, class: 'list-group-item' },
          [
            'z-index',
            'color',
            'background-color',
            'border-color',
          ],
        ),
        inactive: getFrameworkStyle(
          cssRules,
          ['.list-group-item'],
          { node: tabButton, class: 'list-group-item' },
          [
            'color',
            'background-color',
            'border',
          ],
        ),
      },
    };

    const isActive = hasClass(tabButton, BOOTSTRAP_ACTIVE_CLASS);

    let fallbackNode = {};

    const fallbackParams = [
      options.containerTag,
      tabButton,
      buttonClass,
      tabPaneTarget,
      tabGroup,
      isActive,
      componentStyle,
    ];

    let groupNodesFallback;

    if (options.method === 'radio') {
      [fallbackNode, groupNodesFallback]
        = getFallbackNodeRadio(...fallbackParams);
    } else {
      if (options.method === 'target-id') {
        [fallbackNode, groupNodesFallback]
          = getFallbackNodeTargetId(...fallbackParams);
      } else {
        console.err(`Unknown method ${options.method}`);
      }
    }

    groupNodes.push({
      parent: commonAncestor,
      target: firstChild,
      nodes: groupNodesFallback,
    });

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(tabButton, buttonClass);

    groupNodes.push({
      parent: tabButton.parent,
      target: tabButton,
      nodes: fallbackNode,
      shift: 1,
    });

    return tabButton;
  });

  insertNodesAround(groupNodes, tree);
};

export default addTabFallbacks;
