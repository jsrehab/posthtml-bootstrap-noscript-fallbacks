/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

import css from 'css';

import { resetNsfCounter } from './utils.js';
import addDropdownFallbacks from './dropdowns.js';
import addTabFallbacks from './tabs.js';
import addAccordionFallbacks from './accordions.js';
import addOffcanvasFallbacks from './offcanvas.js';
import addCarouselFallbacks from './carousels.js';
import addCollapseFallbacks from './collapse.js';
import addTooltipFallbacks from './tooltips.js';
import addPopoverFallbacks from './popovers.js';
import addAlertFallbacks from './alerts.js';
import addToastFallbacks from './toasts.js';
import addModalFallbacks from './modal.js';
import addAffixFallbacks from './affix.js';

const DEFAULT_CONTAINER_TAG = 'span';

const DEFAULT_OPTIONS = {
  dropdowns: { containerTag: DEFAULT_CONTAINER_TAG },
  tabs: { containerTag: DEFAULT_CONTAINER_TAG, method: 'radio' },
  accordions: { containerTag: DEFAULT_CONTAINER_TAG },
  offcanvas: { containerTag: DEFAULT_CONTAINER_TAG },
  carousels: { containerTag: DEFAULT_CONTAINER_TAG },
  collapse: { containerTag: DEFAULT_CONTAINER_TAG },
  tooltips: { containerTag: DEFAULT_CONTAINER_TAG },
  popovers: { containerTag: DEFAULT_CONTAINER_TAG },
  alerts: { containerTag: DEFAULT_CONTAINER_TAG },
  toasts: { containerTag: DEFAULT_CONTAINER_TAG },
  modal: { containerTag: DEFAULT_CONTAINER_TAG },
  affix: { containerTag: DEFAULT_CONTAINER_TAG },
  cssStylesheet: '',
};

const warnAgainstPositionDependentSelectors = (cssRules) => {
  const possiblyProblematicRules = [];

  for (const cssRule of cssRules) {
    if (!cssRule.selectors) {
      continue;
    }

    for (const selector of cssRule.selectors) {
      if (/:(first|last)-child/.test(selector)) {
        possiblyProblematicRules.push(cssRule);
      }

      if (/:nth-/.test(selector)) {
        possiblyProblematicRules.push(cssRule);
      }
    }
  }

  return possiblyProblematicRules;
};

export default (options) => async tree => {
  let cssRules = [];
  resetNsfCounter();

  // Set default values for missing options
  options = {...DEFAULT_OPTIONS, ...options};

  if (options.cssFiles?.length > 0) {
    const runningInNodeJS = (typeof process !== 'undefined')
      && (process.release.name === 'node');
    if (runningInNodeJS) {
      const url = await import('url');
      const path = await import('path');
      const fs = await import('fs');

      const __dirname = path.dirname(url.fileURLToPath(import.meta.url));

      for (const cssFile of options.cssFiles) {
        const data = fs.readFileSync(path.join(__dirname, cssFile), 'utf8');
        cssRules = cssRules.concat(css.parse(data).stylesheet.rules);
      }
    }
  }

  cssRules = cssRules.concat(css.parse(options.cssStylesheet).stylesheet.rules);

  addDropdownFallbacks(tree, options.dropdowns, cssRules);
  addTabFallbacks(tree, options.tabs, cssRules);
  addAccordionFallbacks(tree, options.accordions, cssRules);
  addOffcanvasFallbacks(tree, options.offcanvas, cssRules);
  addCarouselFallbacks(tree, options.carousels, cssRules);
  addCollapseFallbacks(tree, options.collapse, cssRules);
  addTooltipFallbacks(tree, options.tooltips, cssRules);
  addPopoverFallbacks(tree, options.popovers, cssRules);
  addAlertFallbacks(tree, options.alerts, cssRules);
  addToastFallbacks(tree, options.toasts, cssRules);
  addModalFallbacks(tree, options.modal, cssRules);
  addAffixFallbacks(tree, options.affix, cssRules);
};
