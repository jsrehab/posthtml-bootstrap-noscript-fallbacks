/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const NSF_FALLBACK_CLASS = 'nsf-fallback-el';
const NSF_HAS_FALLBACK_CLASS = 'nsf-has-fallback';

const ARIA_ATTRIBUTE_PATTERN = 'aria-*';
// Bootstrap's default allowlist
// https://getbootstrap.com/docs/5.1/getting-started/javascript/#sanitizer
const SANITIZER_ALLOWLIST = {
  allowedTags: [
    'a',
    'area',
    'b',
    'br',
    'col',
    'code',
    'div',
    'em',
    'hr',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'i',
    'img',
    'li',
    'ol',
    'p',
    'pre',
    's',
    'small',
    'span',
    'sub',
    'sup',
    'strong',
    'u',
    'ul',
  ],
  allowedAttributes: {
    '*': ['class', 'dir', 'id', 'lang', 'role', ARIA_ATTRIBUTE_PATTERN],
    a: ['target', 'href', 'title', 'rel'],
    img: ['src', 'srcset', 'alt', 'title', 'width', 'height'],
  },
};

export {
  NSF_FALLBACK_CLASS,
  SANITIZER_ALLOWLIST,
  NSF_HAS_FALLBACK_CLASS,
};
