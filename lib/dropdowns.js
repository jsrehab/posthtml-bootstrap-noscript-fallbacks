/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  findNodeByClassInList,
  getBS5PseudoClassButtonStyle,
  insertNodesAround,
  pushBody,
} from "./utils.js";

import { NSF_FALLBACK_CLASS } from './common.js';

const addDropdownFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  tree.match([
    { attrs: { 'data-toggle': 'dropdown' } },
    { attrs: { 'data-bs-toggle': 'dropdown' } },
  ], button => {
    // _getMenuElement()
    // First .dropdown-menu following [data-bs-toggle="dropdown"] in the
    // dropdown component
    // https://github.com/twbs/bootstrap/blob/main/js/src/dropdown.js#L283

    if (!button.parent) {
      return button;
    }

    const menu = findNodeByClassInList(
      button.parent.content,
      'dropdown-menu',
    );

    if (!menu) {
      return button;
    }

    // (which is very unlikely, but still)
    const buttonClass = getRandomClass();
    const menuClass = getRandomClass();

    appendClass(menu, menuClass);

    const labelClass = getRandomClass();
    const checkboxId = getRandomClass();

    const btnFocusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');
    const btnActiveStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'active');

    const backdropClass = getRandomClass();

    const containerTag = options.containerTag;

    // autocomplete="off" prevents Firefox from preserving input state across
    // page refresses
    // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
    // Having a pointer cursor on the backdrop (i.e. everywhere on the page when
    // the dropdown is open) is not ideal, but it is better than to have the
    // default cursor on the button in this case
    const fallbackNode = parser(`
      <input type="checkbox" id="${checkboxId}" autocomplete="off"
        style="position:fixed;opacity:0">
      <label role="button" for="${checkboxId}" style="display:none"></label>

      <label role="button" for="${checkboxId}"
        class="${backdropClass} modal-backdrop"
        style="display:none;opacity:0"></label>

      <${containerTag}>
        <style>
          /* Remove the initial button from the document flow */
          .${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: inline-block !important;
            cursor: pointer;
            user-select: none;
          }

          #${checkboxId}:checked ~ .${menuClass} {
            display: block;
          }

          #${checkboxId}:active ~ .${labelClass},
          #${checkboxId}:focus ~ .${labelClass} {
            ${btnFocusStyle}
          }

          #${checkboxId}:active ~ .${labelClass} {
            ${btnActiveStyle}
          }

          .${backdropClass} {
            z-index: 500 !important;
            cursor: pointer;
          }

          #${checkboxId}:checked ~ .${backdropClass} {
            display: block !important;
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the dropdown menu fallback just after the button
    });

    return button;
  });

  insertNodesAround(groupNodes, tree);

  pushBody(
    tree,
    parser(`
      <style>
        .dropdown-menu {
          top: calc(100% + 2px);
        }

        .dropup .dropdown-menu {
          top: initial;
          bottom: calc(100% + 2px);
        }

        .dropend .dropdown-menu {
          top: initial;
          left: calc(100% + 2px);
        }

        .dropstart .dropdown-menu {
          top: initial;
          right: calc(100% + 2px);
        }

        .dropdown-menu-end {
          right: 0;
        }
      </style>
    `),
  );
};

export default addDropdownFallbacks;
