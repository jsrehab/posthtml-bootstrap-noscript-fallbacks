/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  hasClass,
  closestNode,
  getBS5PseudoClassButtonStyle,
  insertNodesAround,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const addAlertFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  tree.match([
    { attrs: { 'data-dismiss': 'alert' } },
    { attrs: { 'data-bs-dismiss': 'alert' } },
  ], button => {
    const buttonClass = getRandomClass();
    const alertClass = getRandomClass();
    const labelClass = getRandomClass();
    const checkboxId = getRandomClass();

    const btnFocusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');
    const btnActiveStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'active');

    const alertElement = closestNode(button, '.alert');
    appendClass(alertElement, alertClass);

    const containerTag = options.containerTag;

    // autocomplete="off" prevents Firefox from preserving input state across
    // page refresses
    // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
    const radioNode = parser(`
      <input type="checkbox" id="${checkboxId}" autocomplete="off"
        style="position:fixed;opacity:0">
    `);

    groupNodes.push({
      parent: alertElement.parent,
      target: alertElement,
      nodes: radioNode,
    });

    const fallbackNode = parser(`
      <label role="button" for="${checkboxId}" style="display:none"></label>

      <${containerTag}>
        <style>
          /* Remove the initial button from the document flow */
          .${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: inline-block !important;
            cursor: pointer;
            user-select: none;
          }

          #${checkboxId}:checked ~ .${alertClass} {
            display: none;
          }

          #${checkboxId}:checked {
            display: none;
          }

          #${checkboxId}:active ~ * .${labelClass},
          #${checkboxId}:focus ~ * .${labelClass} {
            ${btnFocusStyle}
          }

          #${checkboxId}:active ~ * .${labelClass} {
            ${btnActiveStyle}
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the close button fallback just after the button
    });

    return button;
  });

  insertNodesAround(groupNodes, tree);
};

export default addAlertFallbacks;
