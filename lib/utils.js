/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import crypto from 'crypto';

const PREFIX = "nsf-";

let nsfCounter = 0;
const getRandomClass = () => {
  // The class name MUST start with a letter
  return PREFIX + ('' + nsfCounter++).padStart(8, '0');
};

const resetNsfCounter = () => {
  nsfCounter = 0;
};

const appendClass = (element, className) => {
  element.attrs = element.attrs || {};
  element.attrs.class = element.attrs.class ? element.attrs.class + " " : "";
  element.attrs.class += className;
};

const getNodeAttribute = (node, attribute) => {
  if (!node.attrs) {
    return null;
  }

  return node.attrs[attribute];
};

const setNodeAttribute = (node, attribute, value) => {
  node.attrs = node.attrs || {};
  node.attrs[attribute] = value;
  return node;
};

const copyObj = (obj) => {
  if (Array.isArray(obj)) {
    return [...obj];
  }

  return {...obj};
};

const unshiftBody = (tree, nodes) => {
  let bodyFound = false;
  tree.match({ tag: "body" }, node => {
    node.content = node.content || [];
    node.content.unshift(...nodes);
    bodyFound = true;
    return node;
  });

  if (!bodyFound) {
    tree.unshift(...nodes);
  }
};

const pushBody = (tree, nodes) => {
  let bodyFound = false;
  tree.match({ tag: "body" }, node => {
    node.content = node.content || [];
    node.content.push(...nodes);
    bodyFound = true;
    return node;
  });

  if (!bodyFound) {
    tree.push(...nodes);
  }
};

const hasClass = (node, className) => {
  return Boolean(node.attrs)
    && Boolean(node.attrs.class)
    && node.attrs.class.split(' ').includes(className);
};

// Uses a depth-first tree traversal
const findDescendantNodes = (node, predicate, maxNodes = 1000) => {
  const matches = [];

  let stack = [node];
  for (let i = 0; stack.length > 0 && i < maxNodes; i++) {
    const child = stack.pop();

    if (!child) {
      continue;
    }

    if (predicate(child)) {
      matches.push(child);
    }

    if (child.content) {
      stack = stack.concat(child.content.slice().reverse());
    }

    if (Array.isArray(child)) {
      stack = stack.concat(child.slice().reverse());
    }
  }

  return matches;
};

// Tree order is preorder, depth-first traversal of a tree
// https://dom.spec.whatwg.org/#concept-tree-order
const getFirstNodeInTreeOrder = (nodeA, nodeB) => {
  let root = nodeA;
  while (root) {
    if (!root.parent) {
      break;
    }

    root = root.parent;
  }

  let stack = [root];
  while (stack.length > 0) {
    const node = stack.pop();

    if (!node) {
      continue;
    }

    if (node === nodeA) {
      return nodeA;
    }

    if (node === nodeB) {
      return nodeB;
    }

    if (node.content) {
      stack = stack.concat(node.content.slice().reverse());
      continue;
    }

    if (Array.isArray(node)) {
      stack = stack.concat(node.slice().reverse());
    }
  }

  return null;
};

const findCommonAncestor = (nodeA, nodeB, maxHeight = 100) => {
  const firstNodeInTreeOrder = getFirstNodeInTreeOrder(nodeA, nodeB);
  let ancestor = firstNodeInTreeOrder.parent;
  let firstChild = firstNodeInTreeOrder;

  const otherNode = firstNodeInTreeOrder === nodeA ? nodeB : nodeA;

  for (let i = 0; ancestor && i < maxHeight; i++) {
    const containsOtherNode = findDescendantNodes(ancestor, n => {
      return n === otherNode;
    }).length > 0;
    if (containsOtherNode) {
      return [ancestor, firstChild];
    }

    ancestor = ancestor.parent;
    firstChild = firstChild.parent;
  }

  return [null, firstChild];
};

const findNodeByTagInList = (nodeList, tag, nth = 0) => {
  return nodeList.filter(({ tag: t }) => t === tag)[nth];
};

const findNodeByAttrInList = (nodeList, attr) => {
  const [attrName, attrValue] = Object.entries(attr)[0];

  return nodeList.find(({ attrs }) =>
    attrs && Object.entries(attrs)
      .some(([name, value]) => name === attrName && value === attrValue)
  );
};

const findNodeByClassInList = (nodeList, className) => {
  return nodeList.find(({ attrs }) =>
    attrs && attrs.class && attrs.class.split(' ').some(cn => cn === className)
  );
};

const findFirstRule = (rules, selector) => {
  return rules.find(rule => {
    return rule.selectors
      && rule.selectors.some(sel => sel === selector);
  });
};

const stringifyDeclarations = (declarations) => {
  return declarations.map(declaration => {
    return `${declaration.property}:${declaration.value};`;
  }).join('');
};

const getFrameworkStyle = (
  cssRules,
  selectors,
  condition = {},
  cssProperties = 'all',
) => {
  if (condition.node && condition.class) {
    if (!hasClass(condition.node, condition.class)) {
      return '';
    }
  }

  return stringifyDeclarations(selectors.flatMap(sel => {
    const rule = findFirstRule(cssRules, sel);

    if (!rule) {
      return [];
    }

    return rule.declarations;
  }).filter(({ property }) => cssProperties === 'all'
    || cssProperties.includes(property)));
};

// List of ARIA state attributes usually requiring JS to be useful
const ARIA_STATE_JS_ATTRIBUTES = [
  'aria-current',
  'aria-expanded',
  'aria-selected',
];
const removeARIAStateJSAttributes = (node) => {
  if (!node.attrs) {
    return;
  }

  for (const attr of ARIA_STATE_JS_ATTRIBUTES) {
    delete node.attrs[attr];
  }
};

// List of Bootstrap 2, 3, 4 attributes triggering JS behavior
const BS4_JS_ATTRIBUTES = [
  'data-dismiss',
  'data-placement',
  'data-slide',
  'data-slide-to',
  'data-toggle',
];
// List of Bootstrap 5 attributes triggering JS behavior
const BS5_JS_ATTRIBUTES = [
  'data-bs-dismiss',
  'data-bs-placement',
  'data-bs-slide',
  'data-bs-slide-to',
  'data-bs-toggle',
];
const removeBootstrapJSAttributes = (node) => {
  if (!node.attrs) {
    return;
  }

  for (const attr of BS4_JS_ATTRIBUTES.concat(BS5_JS_ATTRIBUTES)) {
    delete node.attrs[attr];
  }
};

const mimicNode = (fallbackNode, fallbackTag, oldNode, copyTag, nth) => {
  const newNode = findNodeByTagInList(fallbackNode, fallbackTag, nth);

  if (copyTag) {
    newNode.tag = oldNode.tag;
  }

  if (oldNode.attrs) {
    newNode.attrs = newNode.attrs || {};
    Object.assign(newNode.attrs, oldNode.attrs);
    delete newNode.attrs.type;
    if (newNode.tag !== 'a') {
      delete newNode.attrs.href;
    }
    removeBootstrapJSAttributes(newNode);
    removeARIAStateJSAttributes(newNode);
  }

  if (oldNode.content) {
    newNode.content = copyObj(oldNode.content);
  }

  return newNode;
};

// https://getbootstrap.com/docs/5.1/components/buttons/
const BOOTSTRAP_BTN_CLASS_LIST = [
  'btn-primary',
  'btn-secondary',
  'btn-success',
  'btn-danger',
  'btn-warning',
  'btn-info',
  'btn-light',
  'btn-dark',

  'btn-link',
  'btn-close',
  'close',

  'btn-outline-primary',
  'btn-outline-secondary',
  'btn-outline-success',
  'btn-outline-danger',
  'btn-outline-warning',
  'btn-outline-info',
  'btn-outline-light',
  'btn-outline-dark',
];

const BOOTSTRAP_BTN_FALLBACK_CLASS = 'btn';

const getBS5PseudoClassButtonStyle = (cssRules, button, pseudoClass) => {
  let buttonBsClass = BOOTSTRAP_BTN_CLASS_LIST.find(className => {
    return hasClass(button, className);
  });

  if (!buttonBsClass
    && hasClass(button, BOOTSTRAP_BTN_FALLBACK_CLASS)) {
    buttonBsClass = BOOTSTRAP_BTN_FALLBACK_CLASS;
  }

  if (buttonBsClass) {
    const rule = findFirstRule(cssRules, `.${buttonBsClass}:${pseudoClass}`);

    if (!rule) {
      return '';
    }

    return stringifyDeclarations(rule.declarations);
  }

  return '';
};

const getMatchingNode = (tree, selector) => {
  let node = null;

  if (!selector) {
    return node;
  }

  const firstCharacter = selector[0];

  const selectorIdentifier = selector.slice(1);
  if (selectorIdentifier === '') {
    return null;
  }

  if (firstCharacter === '#') {
    const idMatcher = { attrs: { id: selectorIdentifier } };
    tree.match(idMatcher, n => node = n);
  }

  if (firstCharacter === '.') {
    tree.walk(n => {
      if (n && hasClass(n, selectorIdentifier)) {
        node = n;
      }
      return n;
    });
  }

  return node;
};

const findChildIndex = (parent, child) => {
  if (Array.isArray(parent)) {
    return parent.indexOf(child);
  }

  return parent.content && parent.content.indexOf(child);
};

const insertNodesAround = (groupNodes, tree) => {
  for (const groupNode of groupNodes) {
    const { parent, target, nodes } = groupNode;
    if (!parent) {
      continue;
    }

    const shift = groupNode.shift || 0;

    if (Array.isArray(parent)) {
      const index = findChildIndex(tree, target);
      const spliceIndex = index + shift >= 0 ? index + shift : 0;

      // Insert the nodes just before the target node
      tree.splice(spliceIndex, 0, ...nodes);
      continue;
    }

    const index = findChildIndex(parent, target);
    const spliceIndex = index + shift >= 0 ? index + shift : 0;

    // Insert the nodes just before the target node
    parent.content.splice(spliceIndex, 0, ...nodes);
  }
};

const matchesSelector = (node, selector) => {
  return selector
    .split(',')
    .some(sel => {
      return node.attrs
        && typeof node.attrs.class === 'string'
        && node.attrs.class.split(' ').some(cl => cl === sel.trim().slice(1));
    });
};

const closestNode = (node, selector) => {
  // Similar to https://developer.mozilla.org/en-US/docs/Web/API/Element/closest

  let oldParent = null;
  let parent = node.parent;

  while (parent && oldParent !== parent) {
    if (matchesSelector(parent, selector)) {
      return parent;
    }

    oldParent = parent;
    parent = parent.parent;
  }

  return null;
};

export {
  getRandomClass,
  resetNsfCounter,
  appendClass,
  getNodeAttribute,
  setNodeAttribute,
  copyObj,
  unshiftBody,
  pushBody,
  hasClass,
  findDescendantNodes,
  findCommonAncestor,
  findNodeByTagInList,
  findNodeByAttrInList,
  findNodeByClassInList,
  findFirstRule,
  stringifyDeclarations,
  getFrameworkStyle,
  removeARIAStateJSAttributes,
  removeBootstrapJSAttributes,
  mimicNode,
  getBS5PseudoClassButtonStyle,
  getMatchingNode,
  findChildIndex,
  insertNodesAround,
  matchesSelector,
  closestNode,
};
