/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  getNodeAttribute,
  insertNodesAround,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const addAffixFallbacks = (tree, options) => {
  const groupNodes = [];

  const containerTag = options.containerTag;

  tree.match([
    { attrs: { 'data-spy': 'affix' } },
  ], affixElement => {
    const affixClass = getRandomClass();

    const fallbackNode = parser(`
      <${containerTag}>
        <style>
          .${affixClass} {
            position: sticky;
          }
        </style>
      </${containerTag}>
    `);

    groupNodes.push({
      parent: affixElement.parent,
      target: affixElement,
      nodes: fallbackNode,
    });

    return affixElement;
  });

  insertNodesAround(groupNodes, tree);
};

export default addAffixFallbacks;
