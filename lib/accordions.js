/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  hasClass,
  findCommonAncestor,
  getFrameworkStyle,
  getMatchingNode,
  insertNodesAround,
} from './utils.js';

import { NSF_FALLBACK_CLASS, NSF_HAS_FALLBACK_CLASS } from './common.js';

const BOOTSTRAP_COLLAPSED_CLASS = 'collapsed';

const getFallbackNodeRadio = (
  containerTag,
  button,
  buttonClass,
  accordionCollapseTarget,
  accordionGroup,
  isCollapsed,
  accordionStyle,
) => {
  const radioGroup = accordionGroup;

  const labelClass = getRandomClass();
  const radioId = getRandomClass();

  const checkedAttributeStr = !isCollapsed ? 'checked' : '';

  // autocomplete="off" prevents Firefox from preserving input state across
  // page refresses
  // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
  const groupNodes = parser(`
    <input type="radio" id="${radioId}" name="${radioGroup}" autocomplete="off"
      style="position:absolute;opacity:0" ${checkedAttributeStr}>
  `);

  const fallbackNode = parser(`
    <label role="button" for="${radioId}" style="display:none"></label>

    <${containerTag}>
      <style>
        .${buttonClass} {
          display: none !important;
        }

        .${labelClass} {
          display: flex !important;
          cursor: pointer;
          font-weight: 400;
        }

        /* Remove Bootstrap active styling */
        #${radioId}:not(:checked) ~ * .${labelClass} {
          ${accordionStyle.collapsed.button}
        }

        #${radioId}:not(:checked) ~ * .${labelClass}::after {
          ${accordionStyle.collapsed.indicator}
        }

        /* Fix Bootstrap .accordion.open (not implemented upstream) */
        /* .accordion.open .collapse {
          display: block;
        }
        .accordion.open .accordion-button {
          color: #0c63e4;
          background-color: #e7f1ff;
          box-shadow: inset 0 -1px 0 rgba(0,0,0,.125);
        }
        .accordion.open .accordion-button::after {
          background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%230c63e4'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e");
          transform: rotate(-180deg);
        } */

        #${radioId}:not(:checked) ~ ${accordionCollapseTarget},
        #${radioId}:not(:checked) ~ * ${accordionCollapseTarget} {
          display: none;
        }

        /* This must have a higher specificity than the rule above removing
         * styling */
        #${radioId}:checked ~ * .${labelClass} {
          ${accordionStyle.shown.button}
        }

        #${radioId}:checked ~ * .${labelClass}::after {
          ${accordionStyle.shown.indicator}
        }

        #${radioId}:checked ~ ${accordionCollapseTarget},
        #${radioId}:checked ~ * ${accordionCollapseTarget} {
          display: block;
        }

        /* Make the focus visible */
        #${radioId}:active ~ * .${labelClass},
        #${radioId}:focus ~ * .${labelClass} {
          ${accordionStyle.focused.button}
        }
      </style>
    </${containerTag}>
  `);

  const label = mimicNode(fallbackNode, 'label', button);
  appendClass(label, labelClass);
  appendClass(label, 'collapsed');
  appendClass(label, NSF_FALLBACK_CLASS);

  return [fallbackNode, groupNodes];
};

const addAccordionFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  const accordionGroups = {};

  tree.match([
    { attrs: { 'data-toggle': 'collapse' } },
    { attrs: { 'data-bs-toggle': 'collapse' } },
  ], accordionButton => {
    if (!accordionButton) {
      return accordionButton;
    }

    if (hasClass(accordionButton, NSF_HAS_FALLBACK_CLASS)) {
      return accordionButton;
    }

    if (!accordionButton.attrs) {
      return accordionButton;
    }

    const accordionCollapseTarget = accordionButton.attrs['data-target']
      || accordionButton.attrs['data-bs-target']
      || accordionButton.attrs.href;

    if (!accordionCollapseTarget) {
      return accordionButton;
    }

    const parent = accordionButton.parent;
    const accordionCollapse = getMatchingNode(tree, accordionCollapseTarget);
    if (!accordionCollapse) {
      return accordionButton;
    }
    const parentNodeSelector = accordionCollapse.attrs
      && (accordionCollapse.attrs['data-parent']
      || accordionCollapse.attrs['data-bs-parent']);

    // If the accordion-collapse has no `data-bs-parent` attribute, the
    // accordion buttons will be handled as collapse buttons
    if (!parentNodeSelector) {
      return accordionButton;
    }

    let accordionGroup = accordionGroups[parentNodeSelector];

    if (!accordionGroup) {
      accordionGroup = `${getRandomClass()}-acc`;
      accordionGroups[parentNodeSelector] = accordionGroup;
    }

    const isCollapsed = hasClass(accordionButton, BOOTSTRAP_COLLAPSED_CLASS);

    const buttonClass = getRandomClass();

    const accordionStyle = {
      shown: {
        button: getFrameworkStyle(
          cssRules,
          ['.accordion-button:not(.collapsed)'],
          { node: accordionButton, class: 'accordion-button' },
          [
            'color',
            'background-color',
            'box-shadow',
          ],
        ),
        indicator: getFrameworkStyle(
          cssRules,
          ['.accordion-button:not(.collapsed)::after'],
          { node: accordionButton, class: 'accordion-button' },
        ),
      },
      collapsed: {
        button: getFrameworkStyle(
          cssRules,
          ['.accordion-button'],
          { node: accordionButton, class: 'accordion-button' },
          [
            'color',
            'background-color',
          ],
        ),
        indicator: getFrameworkStyle(
          cssRules,
          ['.accordion-button::after'],
          { node: accordionButton, class: 'accordion-button' },
        ),
      },
      focused: {
        button: getFrameworkStyle(
          cssRules,
          ['.accordion-button:focus'],
          { node: accordionButton, class: 'accordion-button' },
        ),
      },
    };

    const [fallbackNode, groupNodesRadio] = getFallbackNodeRadio(
      options.containerTag,
      accordionButton,
      buttonClass,
      accordionCollapseTarget,
      accordionGroup,
      isCollapsed,
      accordionStyle,
    );

    const [commonAncestor, firstChild] = findCommonAncestor(
      accordionButton,
      accordionCollapse,
    );

    groupNodes.push({
      parent: commonAncestor,
      target: firstChild,
      nodes: groupNodesRadio,
    });

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(accordionButton, buttonClass);

    appendClass(accordionButton, NSF_HAS_FALLBACK_CLASS);

    groupNodes.push({
      parent,
      target: accordionButton,
      nodes: fallbackNode,
      shift: 1,
    });

    return accordionButton;
  });

  insertNodesAround(groupNodes, tree);
};

export default addAccordionFallbacks;
