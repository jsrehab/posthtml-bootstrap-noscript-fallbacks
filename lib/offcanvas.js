/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  getNodeAttribute,
  setNodeAttribute,
  mimicNode,
  findCommonAncestor,
  getFrameworkStyle,
  getMatchingNode,
  getBS5PseudoClassButtonStyle,
  insertNodesAround,
  closestNode,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const NSF_OFFCANVAS_CHECKBOX_ID_ATTR = 'data-nsf-offcanvas';

const getCheckboxIdFromTargetSelector = (targetSelector) => {
  return `${targetSelector.replace(/#/g, '_')}-chkbox`;
};

const addOffcanvasFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  const offcanvasStyle = {
    shown: {
      offcanvas: getFrameworkStyle(
        cssRules,
        ['.offcanvas.show'],
      ),
      backdrop: getFrameworkStyle(
        cssRules,
        ['.offcanvas-backdrop.show'],
      ),
    },
  };

  const containerTag = options.containerTag;

  tree.match([
    { attrs: { 'data-toggle': 'offcanvas' } },
    { attrs: { 'data-bs-toggle': 'offcanvas' } },
  ], button => {
    if (!button.attrs) {
      return button;
    }

    const offcanvasTarget = (button.attrs['data-target']
      || button.attrs['data-bs-target'])
      || button.attrs.href;

    if (!offcanvasTarget) {
      return button;
    }

    const buttonClass = getRandomClass();

    const labelClass = getRandomClass();

    const btnFocusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');

    const checkboxId = getCheckboxIdFromTargetSelector(offcanvasTarget);

    const offcanvasNode = getMatchingNode(tree, offcanvasTarget);

    if (!offcanvasNode) {
      console.log(button);
      return button;
    }

    setNodeAttribute(offcanvasNode, NSF_OFFCANVAS_CHECKBOX_ID_ATTR, checkboxId);

    // autocomplete="off" prevents Firefox from preserving input state across
    // page refresses
    // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
    let checkboxNode = parser(`
      <input type="checkbox" id="${checkboxId}" autocomplete="off"
        style="position:absolute;opacity:0">
    `);

    if (getNodeAttribute(offcanvasNode, 'data-backdrop') !== 'false'
      || getNodeAttribute(offcanvasNode, 'data-bs-backdrop') !== 'false') {
      const backdropNode = parser(`
        <label role="button" for="${checkboxId}" class="offcanvas-backdrop fade"
          style="display:none"></label>

        <${containerTag}>
          <style>
            #${checkboxId}:checked ~ .offcanvas-backdrop[for="${checkboxId}"] {
              display: block !important;
              ${offcanvasStyle.shown.backdrop}
            }
          </style>
        </${containerTag}>
      `);
      appendClass(backdropNode[1], NSF_FALLBACK_CLASS);

      checkboxNode = checkboxNode.concat(backdropNode);
    }

    const fallbackNode = parser(`
      <label role="button" for="${checkboxId}" style="display:none"></label>

      <${containerTag}>
        <style>
          /* Remove the initial button from the document flow */
          .${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: inline-block !important;
            cursor: pointer;
            user-select: none;
          }

          /* The checkbox and offcanvas are always siblings */
          #${checkboxId}:checked ~ ${offcanvasTarget} {
            visibility: visible;
            ${offcanvasStyle.shown.offcanvas}
          }

          #${checkboxId}:focus ~ .${labelClass},
          #${checkboxId}:focus ~ * .${labelClass} {
            ${btnFocusStyle}
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the offcanvas button fallback just after the button
    });

    const [commonAncestor, firstChild] = findCommonAncestor(
      button,
      offcanvasNode,
    );

    groupNodes.push({
      parent: commonAncestor,
      target: firstChild,
      nodes: checkboxNode,
    });

    return button;
  });

  tree.match([
    { attrs: { 'data-dismiss': 'offcanvas' } },
    { attrs: { 'data-bs-dismiss': 'offcanvas' } },
  ], button => {
    const buttonClass = getRandomClass();

    const labelClass = getRandomClass();

    const btnFocusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');

    const offcanvasNode = closestNode(button, '.offcanvas');

    const checkboxId = offcanvasNode ?
      getNodeAttribute(
        offcanvasNode,
        NSF_OFFCANVAS_CHECKBOX_ID_ATTR,
      )
      : getCheckboxIdFromTargetSelector(getNodeAttribute(button, 'data-target')
        || getNodeAttribute(button, 'data-bs-target'));

    const fallbackNode = parser(`
      <label for="${checkboxId}" style="display:none" role="button"></label>

      <${containerTag} style="position:absolute">
        <style>
          /* Remove the initial button from the document flow */
          .${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: inline-block !important;
            cursor: pointer;
            user-select: none;
          }

          #${checkboxId}:focus ~ * .${labelClass} {
            ${btnFocusStyle}
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the offcanvas close button fallback just after the button
    });

    return button;
  });

  insertNodesAround(groupNodes, tree);
};

export default addOffcanvasFallbacks;
