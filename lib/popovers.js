/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';
import sanitizeHtml from 'sanitize-html';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  getBS5PseudoClassButtonStyle,
  insertNodesAround,
  pushBody,
} from './utils.js';

import { NSF_FALLBACK_CLASS, SANITIZER_ALLOWLIST } from './common.js';

const BOOTSTRAP_PLACEMENT_TABLE = {
  'top': 'top',
  'right': 'end',
  'bottom': 'bottom',
  'left': 'start',
  'auto': 'bottom',
};

const BOOTSTRAP_DEFAULT_POPOVER_PLACEMENT = 'right';

const addPopoverFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  tree.match([
    { attrs: { 'data-toggle': 'popover' } },
    { attrs: { 'data-bs-toggle': 'popover' } },
  ], button => {
    if (!button.attrs) {
      return button;
    }

    let popoverPlacement = button.attrs['data-placement']
      || button.attrs['data-bs-placement'];
    if (!popoverPlacement) {
      popoverPlacement = BOOTSTRAP_DEFAULT_POPOVER_PLACEMENT;
    }
    const bsPosition = `bs-popover-${popoverPlacement}`;
    const bsPositionRTL = `bs-popover-${BOOTSTRAP_PLACEMENT_TABLE[popoverPlacement]}`;

    const buttonClass = getRandomClass();
    const labelClass = getRandomClass();
    const checkboxId = getRandomClass();
    const popoverClass = getRandomClass();

    const originalTitle = sanitizeHtml(button.attrs['title'], SANITIZER_ALLOWLIST);
    button.attrs['data-original-title'] = button.attrs['data-bs-original-title']
      = originalTitle;
    delete button.attrs['title'];

    const popoverTitle = originalTitle ? originalTitle : '';

    const popoverContent = sanitizeHtml(button.attrs['data-content']
      || button.attrs['data-bs-content'], SANITIZER_ALLOWLIST);

    // Both popover-arrow (Bootstrap 5) and arrow (Bootstrap < 5) are used
    // for compatibility
    // Popover placement is added for compatibility with Bootstrap < 5
    // Both positions are added for compatibility with Bootstrap < 5
    const popoverNode = parser(`
      <div class="popover fade ${popoverPlacement} ${bsPosition} ${bsPositionRTL} ${popoverClass}"
        role="tooltip" style="display:none">
        <div class="popover-arrow arrow"></div>
        <h3 class="popover-header popover-title">${popoverTitle}</h3>
        <div class="popover-body popover-content">${popoverContent}</div>
      </div>
    `);

    const btnFocusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');
    const btnActiveStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'active');

    const containerTag = options.containerTag;

    // autocomplete="off" prevents Firefox from preserving input state across
    // page refresses
    // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
    const fallbackNode = parser(`
      <input type="checkbox" id="${checkboxId}" autocomplete="off"
        style="position:fixed;opacity:0">
      <label role="button" for="${checkboxId}"
        style="display:none"></label>

      <${containerTag}>
        <style>
          /* Artificially increase the selector specificity */
          .${buttonClass}.${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: inline-block !important;
            position: relative; /* Enable absolute positionning of the popover */
            cursor: pointer;
          }

          #${checkboxId}:checked ~ * .${popoverClass} {
            display: block !important;
            opacity: 1;
          }

          #${checkboxId}:active ~ .${labelClass},
          #${checkboxId}:focus ~ .${labelClass} {
            ${btnFocusStyle}
          }

          #${checkboxId}:active ~ .${labelClass} {
            ${btnActiveStyle}
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    label.content.push(popoverNode);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the popover fallback just after the button
    });

    appendClass(button, buttonClass);

    return button;
  });

  insertNodesAround(groupNodes, tree);

  pushBody(
    tree,
    parser(`
      <style>
        .popover {
          top: initial !important;
          bottom: initial !important;
          left: initial !important;
          right: initial !important;
        }

        /* Bootstrap < 4 */
        .popover-title:empty {
          display: none;
        }

        .bs-popover-top {
          bottom: calc(100% + 0.5rem + 1px) !important;
        }

        .bs-popover-bottom {
          top: calc(100% + 0.5rem + 1px) !important;
        }

        .bs-popover-end {
          top: 50% !important;
          transform: translateY(-50%) translateX(calc(100% + 0.5rem + 1px));
        }

        .bs-popover-start {
          top: 50% !important;
          transform: translateY(-50%) translateX(calc(-1 * (100% + 0.5rem + 1px)));
        }

        .bs-popover-top > .popover-arrow,
        .bs-popover-bottom > .popover-arrow {
          left: 50%;
          transform: translateX(-50%);
        }

        .bs-popover-end > .popover-arrow,
        .bs-popover-start > .popover-arrow {
          top: 50%;
          transform: translateY(-50%);
        }

        /* Fix the style inherited from the button style, since the popover is
         * now one of its children */
        .popover-header {
          color: rgb(33, 37, 41);
          text-shadow: none;
        }
        .popover-content {
          color: rgb(51, 51, 51);
          text-shadow: none;
        }
      </style>
    `),
  );
};

export default addPopoverFallbacks;
