/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  hasClass,
  findCommonAncestor,
  getMatchingNode,
  getFrameworkStyle,
  getBS5PseudoClassButtonStyle,
  insertNodesAround,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const BOOTSTRAP_COLLAPSED_CLASS = 'collapsed';

const addCollapseFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  const collapseTargetIds = new Map();

  tree.match([
    { attrs: { 'data-toggle': 'collapse' } },
    { attrs: { 'data-bs-toggle': 'collapse' } },
  ], button => {
    if (!button) {
      return button;
    }

    const collapseTargetSel = button.attrs['data-target']
      || button.attrs['data-bs-target']
      || button.attrs.href;

    const collapseTarget = getMatchingNode(tree, collapseTargetSel);
    if (!collapseTarget) {
      return button;
    }
    const parentNodeSelector = collapseTarget.attrs
      && (collapseTarget.attrs['data-parent']
        || collapseTarget.attrs['data-bs-parent']);

    // Skip accordions as they are handled separately
    if (parentNodeSelector) {
      return button;
    }

    const existingCheckboxId = collapseTargetIds.get(collapseTarget);
    const checkboxId = existingCheckboxId ? existingCheckboxId : getRandomClass();

    const buttonClass = getRandomClass();

    const labelClass = getRandomClass();

    if (!existingCheckboxId) {
      collapseTargetIds.set(collapseTarget, checkboxId);

      const isCollapsed = hasClass(button, BOOTSTRAP_COLLAPSED_CLASS)
        || button.attrs['aria-expanded'] === 'false';
      const checkedAttributeStr = !isCollapsed ? 'checked' : '';

      // autocomplete="off" prevents Firefox from preserving input state across
      // page refresses
      // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
      const checkboxNode = parser(`
        <input type="checkbox" id="${checkboxId}" autocomplete="off"
          style="position:fixed;opacity:0" ${checkedAttributeStr}>
      `);

      const [commonAncestor, firstChild] = findCommonAncestor(
        button,
        collapseTarget,
      );

      groupNodes.push({
        parent: commonAncestor,
        target: firstChild,
        nodes: checkboxNode,
      });
    }

    // Get the focus style from accordion buttons when the button is an accordion
    // button
    let focusStyle = getFrameworkStyle(
      cssRules,
      ['.accordion-button:focus'],
      { node: button, class: 'accordion-button' },
    );
    // Get the regular button style otherwise
    if (!focusStyle) {
      focusStyle = getBS5PseudoClassButtonStyle(cssRules, button, 'focus');
    }

    const accordionStyle = {
      shown: {
        button: getFrameworkStyle(
          cssRules,
          ['.accordion-button:not(.collapsed)'],
          { node: button, class: 'accordion-button' },
          [
            'color',
            'background-color',
            'box-shadow',
          ],
        ),
        indicator: getFrameworkStyle(
          cssRules,
          ['.accordion-button:not(.collapsed)::after'],
          { node: button, class: 'accordion-button' },
        ),
      },
      collapsed: {
        button: getFrameworkStyle(
          cssRules,
          ['.accordion-button'],
          { node: button, class: 'accordion-button' },
          [
            'color',
            'background-color',
          ],
        ),
        indicator: getFrameworkStyle(
          cssRules,
          ['.accordion-button::after'],
          { node: button, class: 'accordion-button' },
        ),
      },
    };

    const containerTag = options.containerTag;

    const fallbackNode = parser(`
      <label role="button" for="${checkboxId}"></label>
      <style>
        :where(.${labelClass}) {
          display: none;
        }
        @supports not selector(:where(a)) {
          .${labelClass} {
            display: none;
          }
        }
        /* Make sure the noscript tag is not taken into account in the flow,
         * especially if the parent is a flex element */
        .${labelClass} + style + * {
          display: none !important;
        }
      </style>
      <${containerTag}>
        <style>
          /* Remove the initial button from the document flow */
          .${buttonClass} {
            display: none !important;
          }

          :where(.${labelClass}) {
            display: inline-block;
          }
          @supports not selector(:where(a)) {
            .${labelClass} {
              display: none;
            }
          }

          .${labelClass} {
            cursor: pointer;
          }

          #${checkboxId}:not(:checked) ~ ${collapseTargetSel},
          #${checkboxId}:not(:checked) ~ * ${collapseTargetSel} {
            display: none;
          }

          /* Artificially increase selector specificity over other collapse
           * buttons controlling the same target */
          #${checkboxId}:checked ~ ${collapseTargetSel}${collapseTargetSel},
          #${checkboxId}:checked ~ * ${collapseTargetSel}${collapseTargetSel} {
            display: block;
          }

          #${checkboxId}:active ~ .${labelClass},
          #${checkboxId}:active ~ * .${labelClass},
          #${checkboxId}:focus ~ .${labelClass},
          #${checkboxId}:focus ~ * .${labelClass} {
            ${focusStyle}
          }

          /* Remove Bootstrap active styling */
          #${checkboxId}:not(:checked) ~ .${labelClass},
          #${checkboxId}:not(:checked) ~ * .${labelClass} {
            ${accordionStyle.collapsed.button}
          }

          #${checkboxId}:not(:checked) ~ .${labelClass}::after,
          #${checkboxId}:not(:checked) ~ * .${labelClass}::after {
            ${accordionStyle.collapsed.indicator}
          }

          #${checkboxId}:checked ~ .${labelClass},
          #${checkboxId}:checked ~ * .${labelClass} {
            ${accordionStyle.shown.button}
          }

          #${checkboxId}:checked ~ .${labelClass}::after,
          #${checkboxId}:checked ~ * .${labelClass}::after {
            ${accordionStyle.shown.indicator}
          }
        </style>
      </${containerTag}>
    `);

    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, 'collapsed');
    appendClass(label, NSF_FALLBACK_CLASS);

    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 2,
    });

    return button;
  });

  insertNodesAround(groupNodes, tree);
};

export default addCollapseFallbacks;
