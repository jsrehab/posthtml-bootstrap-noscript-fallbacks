/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  appendClass,
  getNodeAttribute,
  setNodeAttribute,
  mimicNode,
  hasClass,
  findDescendantNodes,
  getFrameworkStyle,
  findChildIndex,
  insertNodesAround,
  closestNode,
} from './utils.js';

import { NSF_FALLBACK_CLASS } from './common.js';

const NSF_CAROUSEL_GROUP_DATA_ATTR = 'data-nsf-carrgroup';

const addCarouselsFallbacks = (tree, options, cssRules) => {
  const groupNodes = [];

  const carouselRadioIds = {};

  const containerTag = options.containerTag;

  const setupCarousel = (carouselNode) => {
    let carouselGroup = getNodeAttribute(
      carouselNode,
      NSF_CAROUSEL_GROUP_DATA_ATTR,
    );

    if (carouselGroup) {
      return;
    }

    carouselGroup = `${getRandomClass()}-car`;
    setNodeAttribute(
      carouselNode,
      NSF_CAROUSEL_GROUP_DATA_ATTR,
      carouselGroup,
    );

    const slides = findDescendantNodes(carouselNode, node => (
      // Bootstrap 4+
      hasClass(node, 'carousel-item')
      // Bootstrap 2, 3
      || hasClass(node, 'item')
    ));

    const carouselId = carouselNode.attrs.id;

    Array.from(slides.entries()).forEach(([i, slide]) => {
      const isActive = hasClass(slide, 'active');
      const checkedAttributeStr = isActive ? 'checked' : '';

      const radioId = getRandomClass();

      const carouselRadioIdData = { slideIndex: i, radioId };

      if (carouselRadioIds[carouselId]) {
        carouselRadioIds[carouselId].push(carouselRadioIdData);
      } else {
        carouselRadioIds[carouselId] = [carouselRadioIdData]
      }

      // autocomplete="off" prevents Firefox from preserving input state across
      // page refresses
      // https://stackoverflow.com/questions/5985839/bug-with-firefox-disabled-attribute-of-input-not-resetting-when-refreshing
      const radioNode = parser(`
        <input type="radio" id="${radioId}" name="${carouselGroup}" autocomplete="off"
          style="position:fixed;opacity:0" ${checkedAttributeStr}>

        <${containerTag}>
          <style>
            #${radioId}:not(:checked) ~ * .carousel-item:nth-child(${i + 1}),
            #${radioId}:not(:checked) ~ * .item:nth-child(${i + 1}) {
              display: none;
            }

            #${radioId}:checked ~ * .carousel-item:nth-child(${i + 1}),
            #${radioId}:checked ~ * .item:nth-child(${i + 1}) {
              display: block;
            }
          </style>
        </${containerTag}>
      `);

      groupNodes.push({
        parent: carouselNode,
        target: carouselNode.content[0],
        nodes: radioNode,
      });
    });
  };

  // Handle slide indicators
  tree.match([
    { attrs: { 'data-slide-to': /./ } },
    { attrs: { 'data-bs-slide-to': /./ } },
  ], button => {
    const carouselNode = closestNode(button, '.carousel');

    if (!carouselNode) {
      return button;
    }

    const buttonClass = getRandomClass();

    const labelClass = getRandomClass();

    const buttonTargetId = (button.attrs['data-target']
      || button.attrs['data-bs-target']
      || button.attrs.href)
      .slice(1); // Remove the leading hash character

    setupCarousel(carouselNode);

    if (!carouselRadioIds[buttonTargetId]) {
      return button;
    }

    const slideTo = parseInt(button.attrs['data-slide-to']
      || button.attrs['data-bs-slide-to']);

    const carouselStyle = {
      active: {
        indicator: getFrameworkStyle(
          cssRules,
          ['.carousel-indicators .active'],
          {},
          ['opacity'],
        ),
      },
      inactive: {
        indicator: getFrameworkStyle(
          cssRules,
          [
            '.carousel-indicators [data-target]',
            '.carousel-indicators [data-bs-target]',
          ],
          {},
          ['opacity'],
        ),
      },
    };

    const carouselRadioIdData = carouselRadioIds[buttonTargetId]
      .find(({ slideIndex }) => slideIndex === slideTo);

    if (!carouselRadioIdData) {
      return button;
    }

    const radioId = carouselRadioIdData.radioId;

    // Slide index depends on the order of `.carousel-item`s
    // https://github.com/twbs/bootstrap/blob/main/js/src/carousel.js#L333

    const fallbackNode = parser(`
      <label role="button" for="${radioId}" style="display:none"></label>

      <${containerTag}>
        <style>
          .${buttonClass} {
            display: none !important;
          }

          .${labelClass} {
            display: flex !important;
            cursor: pointer;
            user-select: none;
          }

          #${radioId}:not(:checked) ~ * .${labelClass} {
            ${carouselStyle.inactive.indicator}
          }

          /* This must have a higher specificity than the rule above removing
            * styling */
          #${radioId}:checked ~ * .${labelClass} {
            ${carouselStyle.active.indicator}
          }
        </style>
      </${containerTag}>
    `);

    // data-target/data-bs-target needs to be kept because it is used by
    // Bootstrap for styling
    const label = mimicNode(fallbackNode, 'label', button);
    appendClass(label, labelClass);
    appendClass(label, NSF_FALLBACK_CLASS);

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(button, buttonClass);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1,
    });

    return button;
  });

  // Handle next/previous arrows
  tree.match([
    { attrs: { 'data-slide': /./ } },
    { attrs: { 'data-bs-slide': /./ } },
  ], button => {
    if (!button.attrs) {
      return button;
    }

    const carouselNode = closestNode(button, '.carousel');

    if (!carouselNode) {
      return button;
    }

    const buttonClass = getRandomClass();

    // Wrap unless disabled
    const shouldWrap = (carouselNode.attrs['data-wrap']
      || carouselNode.attrs['data-bs-wrap']) !== 'false';

    const buttonTargetId = (button.attrs['data-target']
      || button.attrs['data-bs-target']
      || button.attrs.href)
      .slice(1); // Remove the leading hash character

    setupCarousel(carouselNode);

    if (!carouselRadioIds[buttonTargetId]) {
      return button;
    }

    const bsSlide = button.attrs['data-slide'] || button.attrs['data-bs-slide'];

    for (const { slideIndex, radioId } of carouselRadioIds[buttonTargetId]) {
      const labelClass = getRandomClass();

      let slideToTarget = slideIndex;
      if (bsSlide === 'next') {
        slideToTarget++;
      } else {
        if (bsSlide === 'prev') {
          slideToTarget--;
        } else {
          console.err(`Unknown data-slide/data-bs-slide value: ${bsSlide}`);
        }
      }

      const slideCount = carouselRadioIds[buttonTargetId].length;

      if (slideToTarget < 0) {
        slideToTarget = shouldWrap ? slideCount - 1 : 0;
      } else {
        if (slideToTarget >= slideCount) {
          slideToTarget = shouldWrap ? 0 : slideCount - 1;
        }
      }

      const { radioId: radioIdTarget } = carouselRadioIds[buttonTargetId]
        .find(({ slideIndex: i }) => i === slideToTarget);

      const fallbackNode = parser(`
        <label role="button" for='${radioIdTarget}' style='display:none'></label>

        <${containerTag}>
          <style>
            .${buttonClass} {
              display: none !important;
            }

            #${radioId}:checked ~ .${labelClass} {
              display: flex !important;
              cursor: pointer;
              user-select: none;
            }
          </style>
        </${containerTag}>
      `);

      const label = mimicNode(fallbackNode, 'label', button);
      appendClass(label, labelClass);
      appendClass(label, NSF_FALLBACK_CLASS);

      groupNodes.push({
        parent: button.parent,
        target: button,
        nodes: fallbackNode,
        shift: 1,
      });
    }

    // Append the class after the fallback is created, otherwise it is
    // copied in the fallback element
    appendClass(button, buttonClass);

    return button;
  });

  insertNodesAround(groupNodes, tree);
};

export default addCarouselsFallbacks;
