/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { parser } from 'posthtml-parser';
import sanitizeHtml from 'sanitize-html';

import {
  getRandomClass,
  appendClass,
  mimicNode,
  insertNodesAround,
  pushBody,
} from './utils.js';

import { NSF_FALLBACK_CLASS, SANITIZER_ALLOWLIST } from './common.js';

const BOOTSTRAP_PLACEMENT_TABLE = {
  'top': 'top',
  'right': 'end',
  'bottom': 'bottom',
  'left': 'start',
  'auto': 'top',
};

const BOOTSTRAP_DEFAULT_TOOLTIP_PLACEMENT = 'top';

const addTooltipFallbacks = (tree, options) => {
  const groupNodes = [];

  tree.match([
    { attrs: { 'data-toggle': 'tooltip' } },
    { attrs: { 'data-bs-toggle': 'tooltip' } },
  ], button => {
    if (!button.attrs) {
      return button;
    }

    let tooltipPlacement = button.attrs['data-placement']
      || button.attrs['data-bs-placement'];
    if (!tooltipPlacement) {
      tooltipPlacement = BOOTSTRAP_DEFAULT_TOOLTIP_PLACEMENT;
    }
    const bsPosition = `bs-tooltip-${tooltipPlacement}`;
    const bsPositionRTL = `bs-tooltip-${BOOTSTRAP_PLACEMENT_TABLE[tooltipPlacement]}`;

    const buttonClass = getRandomClass();
    const containerClass = getRandomClass();
    const fallbackButtonClass = getRandomClass();
    const tooltipClass = getRandomClass();

    const originalTitle = sanitizeHtml(button.attrs['title'], SANITIZER_ALLOWLIST);
    if (!originalTitle) {
      return button;
    }
    button.attrs['data-original-title'] = button.attrs['data-bs-original-title']
      = originalTitle;
    delete button.attrs['title'];
    const tooltipContent = originalTitle;

    // Both tooltip-arrow (Bootstrap 2, 3, 5) and arrow (Bootstrap 4) are used
    // for compatibility
    // Tooltip placement is added for compatibility with Bootstrap < 5
    // Both positions are added for compatibility with Bootstrap < 5
    const tooltipNode = parser(`
      <div class="tooltip fade ${tooltipPlacement} ${bsPosition} ${bsPositionRTL} ${tooltipClass}"
        role="tooltip">
        <div class="tooltip-arrow arrow"></div>
        <div class="tooltip-inner">${tooltipContent}</div>
      </div>
    `);

    const containerTag = options.containerTag;

    const fallbackNode = parser(`
      <${containerTag}>
        <div class="${containerClass}">
          <button></button>
        </div>

        <style>
          /* Artificially increase the selector specificity */
          .${buttonClass}.${buttonClass} {
            display: none !important;
          }

          .${containerClass} {
            display: inline;
          }

          .${fallbackButtonClass} {
            display: inline-block;
            position: relative; /* Enable absolute positionning of the tooltip */
          }

          .${fallbackButtonClass}:hover .${tooltipClass},
          .${fallbackButtonClass}:focus .${tooltipClass} {
            opacity: 1;
          }

          .${tooltipClass} {
            pointer-events: none;
          }
        </style>
      </${containerTag}>
    `);

    const fallbackButton = mimicNode(
      fallbackNode[1].content[1].content,
      'button',
      button,
      true, // Copy original tag name as well
    );
    fallbackNode[1].content[1].content[1] = fallbackButton;
    fallbackButton.content = fallbackButton.content || [];
    fallbackButton.content.push(tooltipNode);
    appendClass(fallbackButton, fallbackButtonClass);
    appendClass(fallbackButton, NSF_FALLBACK_CLASS);

    groupNodes.push({
      parent: button.parent,
      target: button,
      nodes: fallbackNode,
      shift: 1, // Insert the tooltip fallback just after the button
    });

    appendClass(button, buttonClass);

    return button;
  });

  insertNodesAround(groupNodes, tree);

  pushBody(
    tree,
    parser(`
      <style>
        .bs-tooltip-top {
          bottom: calc(100% + 1px) !important;
        }

        .bs-tooltip-bottom {
          top: calc(100% + 1px) !important;
        }

        .bs-tooltip-end {
          top: 50% !important;
          transform: translateY(-50%) translateX(calc(100% + 1px));
        }

        .bs-tooltip-start {
          top: 50% !important;
          transform: translateY(-50%) translateX(calc(-1 * (100% + 1px)));
        }

        .bs-tooltip-top > .tooltip-arrow,
        .bs-tooltip-bottom > .tooltip-arrow {
          left: 50%;
          transform: translateX(-50%);
        }

        .bs-tooltip-end > .tooltip-arrow,
        .bs-tooltip-start > .tooltip-arrow {
          top: 50%;
          transform: translateY(-50%);
        }
      </style>
    `),
  );
};

export default addTooltipFallbacks;
