<div align="center">
  <img width="150" height="150" alt="PostHTML" src="https://posthtml.github.io/posthtml/logo.svg">
  <h1>JSRehab: generating Bootstrap noscript alternatives with PostHTML</h1>
  <p>Replacing Bootstrap JavaScript components with noscript alternatives</p>
</div>

## Introduction

> This [PostHTML](https://github.com/posthtml/posthtml) plugin automatically generates and injects noscript
alternatives for [Bootstrap 5](https://getbootstrap.com/docs/5.1/components/) or [Bootstrap 4](https://getbootstrap.com/docs/4.6/components) components, when possible.


This repository contains the main code for the WWW '22 Companion paper: ["JSRehab: Weaning Common Web Interface Components from JavaScript Addiction"](https://hal.archives-ouvertes.fr/hal-03604674).

Many Bootstrap components are interface-only components, meaning they only interact with the DOM, only modifying the page appearance in the browser, and can usually be replaced with components not requiring JavaScript.

Provided with the page HTML, this plugin automatically detect Bootstrap components, generates and injects alternatives working without JavaScript.

This plugin aims at preserving accessibility provided by Bootstrap as much as possible, including keyboard and screen reader navigation and screen reader access, and improving it when it makes sense.

This plugin is only compatible with Bootstrap 5 and Bootstap 4 for now.

## Documentation of Noscript Alternatives

Documentation for each noscript alternative can be found in the `doc/` directory.



## Interface

This plugin consumes and produces HTML, and needs to be provided with the page stylesheets to produce the same styling for alternative elements.

## Usage

### Integration in Webpack 5:

Start by cloning this repo into your project directory:
```shell
git clone https://gitlab.inria.fr/jsrehab/posthtml-bootstrap-noscript-fallbacks.git
```

Then, add the following to your existing `package.json` (the vendored packages contain patched
versions needed for this plugin):

```json
"devDependencies": {
  "posthtml-bootstrap-noscript-fallbacks": "file:./posthtml-bootstrap-noscript-fallbacks",
  "posthtml-parser": "file:./posthtml-bootstrap-noscript-fallbacks/vendor/posthtml-parser",
  "posthtml-render": "file:./posthtml-bootstrap-noscript-fallbacks/vendor/posthtml-render",
}
```
and install `posthtml-loader`: `npm install --save-dev posthtml-loader`.

Finally, add the following to your existing `webpack.config.js`:

```js
// Other imports
import posthtmlBootstrapNoscriptFallbacks from "posthtml-bootstrap-noscript-fallbacks";
import { parser } from "posthtml-parser";
import { render } from "posthtml-render";

const config = {
  // Other configuration
  module: {
    rules: [
      // Other rules
      {
        test: /\.html$/,
        use: [
          "extract-loader",
          {
            loader: "html-loader",
            options: { minimize: true },
          },
          {
            loader: "posthtml-loader",
            options: {
              parser,
              render,
              plugins: [
                posthtmlBootstrapNoscriptFallbacks({
                  cssFiles: [
                    // Paths to stylesheets
                  ],
                }),
              ],
            },
          }
        ]
      },
    ],
  },
};
export default config;
```

You may also want to add the paths to your stylesheets in the `cssFiles` plugin option.

### As a standalone PostHTML plugin

```js
import posthtml from 'posthtml';
import posthtmlBootstrapNoscriptFallbacks from 'posthtml-bootstrap-noscript-fallbacks';

posthtml([
    posthtmlBootstrapNoscriptFallbacks({
      cssFiles: [
        // Add paths to your website stylesheets here
        // If you use the default Bootstrap theme, you may use the following, after
        // installing the bootstrap package:
        // "../node_modules/bootstrap/dist/css/bootstrap.css",
      ],
    }),
  ])
  .process('')
  .then(result => console.log(result.html))
```

## Main Accessibility Goals

- Provide consistent tab navigation.
- Make focus at least as visible as with Bootstrap.
- Make it possible to use arrows for components where it makes sense (e.g.,
tabs).
- Be understandable with a screen reader.

## Potential Benefits of Providing noscript Alternatives

- Smaller Time-To-Interactive
- Smaller JavaScript size on the wire
- More responsive interactions
- Less stress on low-end devices, especially mobile devices
- Improving privacy and security of users, by allowing to provide a
  JavaScript-forbidding
  [Content-Security-Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy),
  if no JavaScript is used on the page
- Improving accessibility for users that disable JavaScript
- Allowing users with disabilities to disable JavaScript

## Limitations

Due to limitations when using HTML and CSS alone, this plugin suffers from the
following limitations:

- some keyboards shortcuts are impossible to implement, especially using the
  Escape key to close disclosable elements and using the arrow and the Enter
  keys in some situations,
- `aria-*` state attributes cannot be updated, but since state is managed with standard,
  accessible HTML elements, this is usually a non-issue,
- interacting with some components may result in minor vertical page scrolling,
  this is mitigated as much as possible,
- dynamic positioning of some components (e.g.,
  [tooltips](https://getbootstrap.com/docs/5.1/components/tooltips/) or
  [popovers](https://getbootstrap.com/docs/5.1/components/popovers/)) is not
  possible,
- a very small set of Bootstrap components (e.g.,
  [scrollspy](https://getbootstrap.com/docs/5.1/components/scrollspy/)) cannot
  currently be provided with fallbacks.

## Non-goals

This plugin makes no effort to:

- fix accessibility issues from the existing page, including:
  - adding `aria-*` attributes to HTML elements not generated by the plugin,
  - fixing color contrast of existing HTML elements, including focus indicators,
  - providing focus indicators for HTML elements not generated by the plugin,
  - providing alt text for elements,
  - providing bigger pointer targets,
  - providing bypass blocks.

## License
Licensed under the MIT license (see LICENSE.txt).

Copyright (c) 2022 Inria
