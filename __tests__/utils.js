import posthtml from 'posthtml';
import { parser } from 'posthtml-parser';

import {
  getRandomClass,
  resetNsfCounter,
  appendClass,
  getNodeAttribute,
  setNodeAttribute,
  copyObj,
  unshiftBody,
  pushBody,
  hasClass,
  findDescendantNodes,
  findCommonAncestor,
  findNodeByTagInList,
  findNodeByAttrInList,
  findNodeByClassInList,
  findFirstRule,
  stringifyDeclarations,
  getFrameworkStyle,
  removeARIAStateJSAttributes,
  removeBootstrapJSAttributes,
  mimicNode,
  getBS5PseudoClassButtonStyle,
  getMatchingNode,
  findChildIndex,
  insertNodesAround,
  matchesSelector,
  closestNode,
} from '../lib/utils.js';

test('getRandomClass and resetNsfCounter', () => {
  expect(getRandomClass()).toBe('nsf-00000000');
  expect(getRandomClass()).toBe('nsf-00000001');
  expect(getRandomClass()).toBe('nsf-00000002');

  resetNsfCounter() ;
  expect(getRandomClass()).toBe('nsf-00000000');
  expect(getRandomClass()).toBe('nsf-00000001');
});

test('appendClass', () => {
  const element0 = {};

  appendClass(element0, 'test0');
  expect(element0).toStrictEqual({ attrs: { class: 'test0' }});

  appendClass(element0, 'test1');
  expect(element0).toStrictEqual({ attrs: { class: 'test0 test1' }});

  const element1 = { attrs: {} };

  appendClass(element1, 'test0');
  expect(element1).toStrictEqual({ attrs: { class: 'test0' }});
});

test('getNodeAttribute', () => {
  const node = {
    tag: 'div', attrs: { class: 'test', 'data-bs-toggle': 'tab' },
  };

  expect(getNodeAttribute(node, 'class')).toBe('test');
  expect(getNodeAttribute(node, 'data-bs-toggle')).toBe('tab');

  expect(getNodeAttribute({}, 'class')).toBe(null);
});

test('setNodeAttribute', () => {
  const NODE = {
    tag: 'div',
  };

  const node = JSON.parse(JSON.stringify(NODE));

  expect(setNodeAttribute(
    node,
    'data-nsf-tabgroup',
    'nsf-0000001-tab'
  ).attrs['data-nsf-tabgroup']).toBe('nsf-0000001-tab');

  expect(node.attrs['data-nsf-tabgroup']).toBe('nsf-0000001-tab');

  setNodeAttribute(node, 'data-nsf-tabgroup', 'nsf-0000002-tab');

  expect(node.attrs['data-nsf-tabgroup']).toBe('nsf-0000002-tab');

  setNodeAttribute(node, 'data-nsf-accgroup', 'nsf-0000001-acc');

  expect(node.attrs['data-nsf-accgroup']).toBe('nsf-0000001-acc');
});

test('copyObj', () => {
  const obj = { tag: 'body', content: [ { tag: 'div' } ] };

  expect(copyObj(obj)).toStrictEqual(obj);
  expect(copyObj(obj)).not.toBe(obj);

  const arr = [0, 1, 2];
  expect(copyObj(arr)).toStrictEqual(arr);
  expect(copyObj(arr)).not.toBe(arr);
});

test('unshiftBody', async () => {
  const tree0 = (await posthtml().process('<body></body>')).tree;
  const nodes = [ { tag: 'header' }, { tag: 'span' } ];

  unshiftBody(tree0, nodes);
  expect(tree0[0].content).toHaveLength(2);
  expect(tree0[0].content[0].tag).toBe('header');

  const tree1 = (await posthtml().process('<body><div></div></body>')).tree;

  unshiftBody(tree1, nodes);
  expect(tree1[0].content).toHaveLength(3);
  expect(tree1[0].content[0].tag).toBe('header');
  expect(tree1[0].content[2].tag).toBe('div');

  const tree2 = (await posthtml().process('<div></div><section></section>')).tree;

  unshiftBody(tree2, nodes);
  expect(tree2).toHaveLength(4);
  expect(tree2[0].tag).toBe('header');
  expect(tree2[3].tag).toBe('section');
});

test('pushBody', async () => {
  const tree0 = (await posthtml().process('<body></body>')).tree;
  const nodes = [ { tag: 'header' }, { tag: 'span' } ];

  pushBody(tree0, nodes);
  expect(tree0[0].content).toHaveLength(2);
  expect(tree0[0].content[0].tag).toBe('header');

  const tree1 = (await posthtml().process('<body><div></div></body>')).tree;

  pushBody(tree1, nodes);
  expect(tree1[0].content).toHaveLength(3);
  expect(tree1[0].content[0].tag).toBe('div');
  expect(tree1[0].content[2].tag).toBe('span');

  const tree2 = (await posthtml().process('<div></div><section></section>')).tree;

  pushBody(tree2, nodes);
  expect(tree2).toHaveLength(4);
  expect(tree2[0].tag).toBe('div');
  expect(tree2[3].tag).toBe('span');
});

test('hasClass', () => {
  const node0 = {};
  expect(hasClass(node0, 'test0')).toBe(false);

  const node1 = { attrs: {} };
  expect(hasClass(node1, 'test0')).toBe(false);

  const node2 = { attrs: { class: '' } };
  expect(hasClass(node2, 'test0')).toBe(false);

  const node3 = { attrs: { class: 'test0' } };
  expect(hasClass(node3, 'test0')).toBe(true);
  expect(hasClass(node3, 'test1')).toBe(false);

  const node4 = { attrs: { class: 'test0-test1 test2 test3' } };
  expect(hasClass(node4, 'test2')).toBe(true);
  expect(hasClass(node4, 'test0')).toBe(false);
});

test('findDescendantNodes', () => {
  const node = {
    tag: 'div',
    content: [
      { tag: 'span' },
      {
        tag: 'span',
        content: [
          {
            tag: 'div',
            content: [
              { tag: 'button', attrs: { 'data-bs-toggle': 'collapse' } },
            ],
          },
        ],
      },
      {
        tag: 'span',
        content: [
          {
            tag: 'button',
            attrs: { 'data-bs-toggle': 'collapse', 'class': '1' },
          },
        ],
      },
    ],
  };

  const nodesFound = findDescendantNodes(node, child =>
    child.attrs && child.attrs['data-bs-toggle'] === 'collapse'
  );
  expect(nodesFound).toHaveLength(2);

  expect(nodesFound[0]).toBe(node.content[1].content[0].content[0]);
  expect(nodesFound[1]).toBe(node.content[2].content[0]);
});

test('findCommonAncestor', async () => {
  const tree0 = (await posthtml().process(`
    <div>
      <span></span>
      <div>
        <span>
          <button data-bs-toggle="dropdown"></button>
        </span>
      </div>
      <section>
        <button data-bs-toggle="collapse"></button>
      </section>
    </div>
  `, { parser })).tree;

  // findDescendantNodes uses a depth-first tree traversal
  const nodesWithParents = findDescendantNodes(
    tree0[1],
    n => n.tag === 'button',
  );

  const [node0, node1] = findCommonAncestor(
    nodesWithParents[1],
    nodesWithParents[0],
  );

  expect(node0).toBe(tree0[1]);
  expect(node1).toBe(tree0[1].content[3]);

  // Must work even if nodeB comes before nodeA in tree order
  const [node2, node3] = findCommonAncestor(
    nodesWithParents[0],
    nodesWithParents[1],
  );

  expect(node2).toBe(tree0[1]);
  expect(node3).toBe(tree0[1].content[3]);
});

test('findNodeByTagInList', () => {
  const nodes = [
    { tag: 'div' },
    {},
    { tag: 'span' },
    { tag: 'div' },
    { tag: 'span' },
    { tag: 'div' },
  ];

  expect(findNodeByTagInList(nodes, 'span')).toBe(nodes[2]);
});

test('findNodeByAttrInList', () => {
  const nodes = [
    { tag: 'div' },
    { tag: 'div', attrs: {} },
    { tag: 'div', attrs: { 'data-bs-toggle': 'dropdown' } },
    { tag: 'div', attrs: { 'data-toggle': 'collapse' } },
    { tag: 'div', attrs: { 'data-bs-toggle': 'collapse' } },
    { tag: 'div' },
  ];

  expect(findNodeByAttrInList(
    nodes,
    { 'data-bs-toggle': 'collapse' },
  )).toBe(nodes[4]);
});

test('findNodeByClassInList', () => {
  const nodes = [
    { tag: 'div' },
    { tag: 'div', attrs: {} },
    { tag: 'div', attrs: { class: '' } },
    { tag: 'div', attrs: { class: 'dropdown' } },
    { tag: 'div', attrs: { class: 'dropdown-menu' } },
    { tag: 'div' },
  ];

  expect(findNodeByClassInList(
    nodes,
    'dropdown-menu',
  )).toBe(nodes[4]);
});

test('findFirstRule', () => {
  const rules = [
    { selectors: [] },
    {},
    { selectors: ['.test-class'] },
    { selectors: ['p', '.test-class ul'] },
    { selectors: [] },
  ];

  expect(findFirstRule(rules, '.test-class ul')).toBe(rules[3]);
});

test('stringifyDeclarations', () => {
  const declarations = [
    { property: 'color', value: 'red' },
    { property: 'font-size', value: '12px' },
    { property: 'display', value: 'none' },
  ];

  expect(stringifyDeclarations(declarations))
    .toBe('color:red;font-size:12px;display:none;');
});

test('getFrameworkStyle', () => {
  const cssRules = [
    {
      selectors: ['p'],
      declarations: [ { property: 'color', value: 'green' } ],
    },
    {
      selectors: ['.accordion-button'],
      declarations: [
        { property: 'color', value: 'red' },
        { property: 'background-color', value: 'white' },
        { property: 'display', value: 'none' },
      ],
    },
    {
      selectors: ['div'],
      declarations: [ { property: 'color', value: 'blue' } ],
    },
  ];

  const button = {
    tag: 'button',
    attrs: {
      class: 'btn accordion-button',
    },
  };

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
    [
      'color',
      'background-color',
    ],
  )).toBe('color:red;background-color:white;');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
  )).toBe('color:red;background-color:white;display:none;');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
    'all',
  )).toBe('color:red;background-color:white;display:none;');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
    [],
  )).toBe('');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
    [ 'non-existing-property' ],
  )).toBe('');

  expect(getFrameworkStyle(
    cssRules,
    [],
    { node: button, class: 'accordion-button' },
    'other_string',
  )).toBe('');

  expect(getFrameworkStyle(
    cssRules,
    ['.other-selector'],
    { node: button, class: 'accordion-button' },
  )).toBe('');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: button, class: 'accordion-button' },
    'other_string',
  )).toBe('');

  expect(getFrameworkStyle(
    cssRules,
    ['.accordion-button'],
    { node: {}, class: 'accordion-button' },
  )).toBe('');
});

test('removeARIAStateJSAttributes', () => {
  const node = {
    attrs: {
      class: 'test',
      'aria-expanded': 'false',
      'aria-selected': 'true',
    },
  };

  removeARIAStateJSAttributes(node);
  expect(node).toStrictEqual({ attrs: { class: 'test' } });

  removeARIAStateJSAttributes({});
});

test('removeBootstrapJSAttributes', () => {
  const node = {
    attrs: {
      class: 'test',
      'data-bs-toggle': 'collapse',
      'data-bs-dismiss': 'offcanvas',
    },
  };

  removeBootstrapJSAttributes(node);
  expect(node).toStrictEqual({ attrs: { class: 'test' } });

  removeBootstrapJSAttributes({});
});

test('mimicNode', () => {
  const oldNode = {
    tag: 'a',
    attrs: {
      class: 'test0',
      href: '/test.html',
      'data-bs-toggle': 'collapse',
      'aria-expanded': 'false',
    },
    content: [
      { text: 'Test button' },
      { tag: 'div' },
    ],
  };

  const fallbackNode = [
    { text: '\n' },
    {
      tag: 'label',
      attrs: {
        class: 'test0 test1',
      },
    },
  ];

  // FIXME: test how the parent property is handled

  const newNode = mimicNode(fallbackNode, 'label', oldNode);

  expect(newNode).toStrictEqual({
    tag: 'label',
    attrs: {
      class: 'test0',
    },
    content: [
      { text: 'Test button' },
      { tag: 'div' },
    ],
  });
  expect(newNode).not.toBe(oldNode);

  expect(mimicNode([{ tag: 'label' }], 'label', {}))
    .toStrictEqual({ tag: 'label' });

  expect(mimicNode(
    [{ tag: 'label' }],
    'label',
    { tag: 'button', attrs: { class: 'btn' } },
  )).toStrictEqual({ tag: 'label', attrs: { class: 'btn' } });
});

test('getBS5PseudoClassButtonStyle', () => {
  const cssRules = [
    {
      selectors: ['p'],
      declarations: [ { property: 'color', value: 'green' } ],
    },
    {
      selectors: ['.btn-secondary:focus'],
      declarations: [
        { property: 'color', value: 'red' },
        { property: 'background-color', value: 'white' },
      ],
    },
    {
      selectors: ['.btn:focus'],
      declarations: [
        { property: 'color', value: 'orange' },
        { property: 'background-color', value: 'wheat' },
      ],
    },
    {
      selectors: ['div'],
      declarations: [ { property: 'color', value: 'blue' } ],
    },
  ];

  const button = {
    tag: 'button',
    attrs: {
      class: 'btn btn-secondary',
    },
  };

  expect(getBS5PseudoClassButtonStyle(cssRules, button, 'focus'))
    .toBe('color:red;background-color:white;');

  expect(getBS5PseudoClassButtonStyle(
    cssRules,
    { tag: 'button', attrs: { class: 'btn' } },
    'focus',
  )).toBe('color:orange;background-color:wheat;');

  expect(getBS5PseudoClassButtonStyle([], button, 'focus')).toBe('');

  expect(getBS5PseudoClassButtonStyle([], {}, 'focus')).toBe('');
});

test('getMatchingNode', async () => {
  const html0 = '<body><div id=""></div><div id><span class="test"></span><span id="test"></span></div></body>';
  const tree0 = (await posthtml().process(html0)).tree;
  expect(getMatchingNode(tree0, '#test')).toBe(tree0[0].content[1].content[1]);
  expect(getMatchingNode(tree0, '#absent')).toBeNull();
  expect(getMatchingNode(tree0, '.test')).toBe(tree0[0].content[1].content[0]);
  expect(getMatchingNode(tree0, '.absent')).toBeNull();

  expect(getMatchingNode(tree0, '#')).toBeNull();
});

test('findChildIndex', () => {
  const node = {
    tag: 'div',
    content: [
      { tag: 'div' },
      { text: '\n' },
      { tag: 'span', attrs: { class: 'test' }, content: [{ tag: 'article' }] },
      { text: '\n' },
      { tag: 'section' },
    ],
  };

  expect(findChildIndex(
    node,
    node.content[2],
  )).toBe(2);

  expect(findChildIndex(
    node,
    node.content[3],
  )).toBe(3);
});

test('insertNodesAround', () => {
  const NODE = {
    tag: 'div',
    content: [
      { tag: 'div' },
      { text: '\n' },
      { tag: 'span' },
      { text: '\n' },
      { tag: 'section' },
    ],
  };

  const node0 = JSON.parse(JSON.stringify(NODE));

  const newNodes0 = [
    { tag: 'img' },
    { tag: 'a' },
  ];

  const newNodes1 = [
    { tag: 'picture' },
    { tag: 'script' },
  ];

  const newNodes2 = [
    { tag: 'h2' },
    { tag: 'style' },
  ];

  insertNodesAround([
    {
      parent: node0,
      target: node0.content[2],
      nodes: newNodes0,
    },
    {
      parent: node0,
      target: node0.content[4],
      nodes: newNodes1,
      shift: 1,
    },
  ]);

  expect(node0.content[1].text).toBe('\n');
  expect(node0.content[2].tag).toBe('img');
  expect(node0.content[3].tag).toBe('a');
  expect(node0.content[4].tag).toBe('span');

  expect(node0.content[7].tag).toBe('picture');
  expect(node0.content[8].tag).toBe('script');

  const node1 = JSON.parse(JSON.stringify(NODE));

  insertNodesAround([
    {
      parent: node1,
      target: node1.content[0],
      nodes: newNodes0,
      shift: -1,
    },
    {
      parent: node1,
      target: node1.content[2],
      nodes: newNodes1,
      shift: -1,
    },
    {
      parent: node1,
      target: node1.content[4],
      nodes: newNodes2,
      shift: 2,
    },
  ]);

  // Index should clamp to 0 when negative
  expect(node1.content[0].tag).toBe('img');
  expect(node1.content[1].tag).toBe('a');
  expect(node1.content[2].tag).toBe('div');

  expect(node1.content[3].tag).toBe('picture');
  expect(node1.content[4].tag).toBe('script');

  expect(node1.content[9].tag).toBe('h2');
  expect(node1.content[10].tag).toBe('style');

  const nodeArray = [
    { tag: 'div' },
    { tag: 'span' },
    { tag: 'article' },
  ];

  insertNodesAround([
    {
      parent: nodeArray,
      target: nodeArray[1],
      nodes: newNodes0,
    },
    {
      parent: nodeArray,
      target: nodeArray[1],
      nodes: newNodes1,
      shift: 1,
    },
  ], nodeArray);

  expect(nodeArray[0].tag).toBe('div');
  expect(nodeArray[1].tag).toBe('img');
  expect(nodeArray[2].tag).toBe('a');
  expect(nodeArray[3].tag).toBe('span');

  expect(nodeArray[3].tag).toBe('span');
  expect(nodeArray[4].tag).toBe('picture');
  expect(nodeArray[5].tag).toBe('script');
  expect(nodeArray[6].tag).toBe('article');
});

test('matchesSelector', () => {
  const node0 = {
    tag: 'div',
    attrs: { class: 'nav-pills nav' },
  };

  const node1 = {
    tag: 'div',
    attrs: { class: 'nav-pills' },
  };

  expect(matchesSelector(node0, '.test,.nav')).toBe(true);
  expect(matchesSelector(node0, '.test, .nav')).toBe(true);
  expect(matchesSelector(node1, '.nav')).toBe(false);

  expect(matchesSelector(node1, '.nav-pills')).toBe(true);

  expect(matchesSelector({}, '')).toBe(false);
});

test('closestNode', async () => {
  const tree0 = (await posthtml().process(`
    <div class="nav bar">
      <span></span>
      <article class="nav-pills nav">
        <div class="nav-pills">
          <span>
            <button class="nav"></button>
          </span>
        </div>
      </div>
      <span></span>
    </div>
  `, { parser })).tree;

  expect(closestNode(
    tree0[1].content[3].content[1].content[1].content[1],
    '.test, .nav',
  )).toBe(tree0[1].content[3]);

  expect(closestNode(
    tree0[1].content[3].content[1].content[1].content[1],
    '.notfound',
  )).toBe(null);
});
