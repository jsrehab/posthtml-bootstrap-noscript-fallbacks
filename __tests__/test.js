import plugin from '../lib/index.js';
import posthtml from 'posthtml';

import { fileURLToPath } from "url";
import path from 'path';
import { readFileSync, writeFileSync, mkdirSync } from 'fs';
import { parser } from 'posthtml-parser';
import { render } from 'posthtml-render';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const fixture = file => readFileSync(path.join(__dirname, 'fixtures', `${file}.html`), 'utf8');

const clean = html => html.replace(/[^\S\r\n]+$/gm, '').trim();

const OUTPUT_DIR = 'generated';

const process = async (name, file, options, log = false) => {
  const result = await posthtml([plugin(options)])
    .process(fixture(file), {parser, render});
  const html = log ? console.log(result.html) : clean(result.html);

  // TODO: create the output directory only once
  mkdirSync(path.join(__dirname, OUTPUT_DIR), { recursive: true });
  writeFileSync(path.join(__dirname, OUTPUT_DIR, `${name}.html`), html, 'utf8');

  return expect(html).toMatchSnapshot();
};

const cssFiles = ["../node_modules/bootstrap/dist/css/bootstrap.css"];

const TESTS = [
  {
    name: 'bootstrap_doc_example_dropdowns',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_collapse',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_accordion',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_carousel',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_offcanvas',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_navs-tabs_radio',
    file: 'bootstrap_doc_example_navs-tabs',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_navs-tabs_target-id',
    file: 'bootstrap_doc_example_navs-tabs',
    options: {
      cssFiles,
      tabs: { method: 'target-id' },
    },
  },
  {
    name: 'bootstrap_doc_example_tooltips',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_popovers',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_alerts',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_toasts',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_doc_example_modal',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_carousel',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_collapse',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_dropdown',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_popover',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_tab_radio',
    file: 'bootstrap_visual_test_tab',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_tab_target-id',
    file: 'bootstrap_visual_test_tab',
    options: {
      cssFiles,
      tabs: { method: 'target-id' },
    },
  },
  {
    name: 'bootstrap_visual_test_tooltip',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_alert',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_toast',
    options: { cssFiles },
  },
  {
    name: 'bootstrap_visual_test_modal',
    options: { cssFiles },
  },
];

const onlyTests = TESTS.filter(testCase => testCase.only);
const testsToRun = onlyTests.length > 0 ? onlyTests : TESTS;

for (const testCase of testsToRun) {
  test(testCase.name, () => {
    const file = testCase.file || testCase.name;
    return process(testCase.name, file, testCase.options);
  });
}
